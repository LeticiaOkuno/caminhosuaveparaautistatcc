﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
//adicionados as seguintes bibliotecas:
using Coding4Fun.Kinect.Wpf.Controls;
using System.Windows.Threading;
using Microsoft.Kinect;

namespace IFSP_Caminho_Suave.Janelas
{
    /// <summary>
    /// Interaction logic for Nivel_2_0.xaml
    /// </summary>
    public partial class Nivel_2_0 : Window
    {
        #region Atributos
        private bool _bocaClicada = false;
        private bool _olhoDireitoClicado = false;
        private bool _olhoEsquerdoClicado = false;
        private bool _narizClicado = false;
        private bool _objetoBocaClicada = false;
        private bool _objetoOlhoDireitoClicado = false;
        private bool _objetoOlhoEsquerdoClicado = false;
        private bool _objetoNarizClicado = false;
        private static int larguraEcrã = 1500;
        private static int alturaTela = 1000;
        private KinectSensor _sensor;
        private Skeleton[] todosEsqueletos = new Skeleton[6];
        private List<HoverButton> botoesKinect;
        #endregion Atributos

        #region Janela principal
        public Nivel_2_0()
        {
            InitializeComponent();
            botoesKinect = new List<HoverButton>()
            {
                soltarBoca,
                soltarOlhoDireito,
                soltarOlhoEsquerdo,
                soltarNariz,
                seleBoca,
                seleOlhoDireito,
                seleOlhoEsquerdo,                
                seleNariz
          };
        }
        #endregion Janela principal

        #region Captura
        void _sensor_AllFramesReady(object sender, AllFramesReadyEventArgs e)
        {
            Skeleton first = metdodos.GetFirstSkeleton(e);
            if (first == null)
            {
                return;
            }
            metdodos.ScalePosition(HandCursor, first.Joints[JointType.HandRight]);
            ProcessGesture(first.Joints[JointType.HandRight]);
            GetCameraPoint(first, e);
        }
        Classes.MetodosEPropriedades metdodos = new Classes.MetodosEPropriedades();
        void GetCameraPoint(Skeleton first, AllFramesReadyEventArgs e)
        {
            using (DepthImageFrame depth = e.OpenDepthImageFrame())
            {
                if (depth == null || _sensor == null)
                {
                    return;
                }
                DepthImagePoint rightDepthPoint =
                    depth.MapFromSkeletonPoint(first.Joints[JointType.HandRight].Position);
                ColorImagePoint rightColorPoint =
                    depth.MapToColorImagePoint(rightDepthPoint.X * 6, rightDepthPoint.Y * 3,
                        ColorImageFormat.RgbResolution640x480Fps30);
                metdodos.CameraPosition(HandCursor, rightColorPoint);
                foreach (HoverButton hb in botoesKinect)
                {
                    metdodos.CheckButton(hb, HandCursor);
                }
            }
        }
        #endregion Captura

        #region Tempo
        private int incremento;
        private void Dt_Tick(object sender, EventArgs e)
        {
            incremento++;


            if (incremento == 3)
            {
          //      Nivel_2_1 n21 = new Nivel_2_1();
              //  n21.ShowDialog();
            }
        }
        #endregion Tempo

        #region Processamentos
        private void ProcessGesture(Joint rightHand)
        {
            SkeletonPoint point = new SkeletonPoint();
            point.X = metdodos.ScaleVector(larguraEcrã, rightHand.Position.X);
            point.Y = metdodos.ScaleVector(alturaTela, -rightHand.Position.Y);
            point.Z = rightHand.Position.Z;
            rightHand.Position = point;
            if (_bocaClicada && !_objetoBocaClicada)
            {
                Canvas.SetLeft(souBoca, rightHand.Position.X / 1);
                Canvas.SetTop(souBoca, rightHand.Position.Y / 1.9);
            }
            if (_olhoDireitoClicado && !_objetoOlhoDireitoClicado)
            {
                Canvas.SetLeft(souOlhoDireito, rightHand.Position.X / 1);
                Canvas.SetTop(souOlhoDireito, rightHand.Position.Y / 1.9);
            }
            if (_olhoEsquerdoClicado && !_objetoOlhoEsquerdoClicado)
            {
                Canvas.SetLeft(souOlhoEsquerdo, rightHand.Position.X / 1);
                Canvas.SetTop(souOlhoEsquerdo, rightHand.Position.Y / 1.9);
            }
            if (_narizClicado && !_objetoNarizClicado)
            {
                Canvas.SetLeft(souNariz, rightHand.Position.X / 1);
                Canvas.SetTop(souNariz, rightHand.Position.Y / 1.9);
            }
            if (rightHand.Position.Z > 0.70)
            {
                txtAviso.Visibility = Visibility.Visible;
                txtAviso.Text = "Venha mais para frente";
            }
        }
        #endregion Processamentos

        #region Carregar e fechar janelas 
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
           
            this.Top = 0;
            this.Left = 0;
            //  this.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            txtPonto.Visibility = System.Windows.Visibility.Hidden;
            txtPonto1.Visibility = System.Windows.Visibility.Hidden;
            souBoca.Visibility = System.Windows.Visibility.Hidden;
            souOlhoDireito.Visibility = System.Windows.Visibility.Hidden;
            souOlhoEsquerdo.Visibility = System.Windows.Visibility.Hidden;
            /*   if (KinectSensor.KinectSensors.Count > 0)
               {
                   txtAviso.Visibility = Visibility.Hidden;
                   _sensor = KinectSensor.KinectSensors[0];
                   if (_sensor.Status == KinectStatus.Connected)
                   {
                       _sensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
                       _sensor.AllFramesReady += new EventHandler<AllFramesReadyEventArgs>(_sensor_AllFramesReady);
                       var parameters = new TransformSmoothParameters
                       {
                           Smoothing = 0.3f,
                           Correction = 0.0f,
                           Prediction = 0.0f,
                           JitterRadius = 1.0f,
                           MaxDeviationRadius = 0.5f
                       };
                       _sensor.SkeletonStream.Enable(parameters);
                       _sensor.Start();
                       System.Media.SoundPlayer estruRosto = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/EstruRosto.wav");
                       estruRosto .Play();
                   }
               }*/
           
           
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            if (_sensor != null)
            {
             //   _sensor.Stop();
            }
        }

        #endregion Carregar e fechar janelas 

        #region Eventos
        System.Media.SoundPlayer parabens = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/Parabens.wav");
       System.Media.SoundPlayer naoFoiDessaVez = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/NaoFoiDessaVez.wav");
        Classes.ContaPontos contaPontos = new Classes.ContaPontos();
        
        private void seleBoca_Click(object sender, RoutedEventArgs e)
        {
            

        
            HandCursor.Fill = new System.Windows.Media.ImageBrush(new System.Windows.Media.Imaging.BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Mao/Mao Boca.png", UriKind.Relative)));
            if (_objetoBocaClicada == true)
            {
                seleBoca.IsEnabled = false;
            }
            else
            {
                _bocaClicada = true;
                seleBoca.Visibility = System.Windows.Visibility.Hidden;
                souBoca.Visibility = System.Windows.Visibility.Hidden;
                if (_bocaClicada == true)
                {
                    _olhoDireitoClicado = false;
                    _olhoEsquerdoClicado = false;
                    _narizClicado = false;
                    if (_objetoOlhoDireitoClicado == false)
                    {
                        seleOlhoDireito.Visibility = System.Windows.Visibility.Visible;
                        souOlhoDireito.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoOlhoDireitoClicado == true)
                    {
                        seleOlhoDireito.IsEnabled = false;
                        seleOlhoDireito.Visibility = System.Windows.Visibility.Hidden;
                        souOlhoDireito.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoOlhoEsquerdoClicado == false)
                    {
                        seleOlhoEsquerdo.Visibility = System.Windows.Visibility.Visible;
                        souOlhoEsquerdo.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoOlhoEsquerdoClicado == true)
                    {
                        seleOlhoEsquerdo.IsEnabled = false;
                        seleOlhoEsquerdo.Visibility = System.Windows.Visibility.Hidden;
                        souOlhoEsquerdo.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoNarizClicado == false)
                    {
                        seleNariz.Visibility = System.Windows.Visibility.Visible;
                        souNariz.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoNarizClicado == true)
                    {
                        seleNariz.IsEnabled = false;
                        seleNariz.Visibility = System.Windows.Visibility.Hidden;
                        souNariz.Visibility = System.Windows.Visibility.Visible;
                    }
                }

                System.Media.SoundPlayer boca = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/Boca.wav");
                boca.Play();
            }
        }
          private void seleOlhoDireito_Click(object sender, RoutedEventArgs e)
        {
            
            this.Close();
            HandCursor.Fill = new System.Windows.Media.ImageBrush(new System.Windows.Media.Imaging.BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Mao/Mao Olho Direito.png", UriKind.Relative)));
            if (_objetoOlhoDireitoClicado == true)
            {
                seleOlhoDireito.IsEnabled = false;
            }
            else
            {
                _olhoDireitoClicado = true;
                seleOlhoDireito.Visibility = System.Windows.Visibility.Hidden;
                souOlhoDireito.Visibility = System.Windows.Visibility.Hidden;
                if (_olhoDireitoClicado == true)
                {
                    _bocaClicada = false;
                    _olhoEsquerdoClicado = false;
                    _narizClicado = false;
                    if (_objetoBocaClicada == false)
                    {
                        seleBoca.Visibility = System.Windows.Visibility.Visible;
                        souBoca.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoBocaClicada == true)
                    {
                        seleBoca.IsEnabled = false;
                        seleBoca.Visibility = System.Windows.Visibility.Hidden;
                        souBoca.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoOlhoEsquerdoClicado == false)
                    {
                        seleOlhoEsquerdo.Visibility = System.Windows.Visibility.Visible;
                        souOlhoEsquerdo.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoOlhoEsquerdoClicado == true)
                    {
                        seleOlhoEsquerdo.IsEnabled = false;
                        seleOlhoEsquerdo.Visibility = System.Windows.Visibility.Hidden;
                        souOlhoEsquerdo.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoNarizClicado == false)
                    {
                        seleNariz.Visibility = System.Windows.Visibility.Visible;
                        souNariz.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoNarizClicado == true)
                    {
                        seleNariz.IsEnabled = false;
                        seleNariz.Visibility = System.Windows.Visibility.Hidden;
                        souNariz.Visibility = System.Windows.Visibility.Visible;
                    }
                }

                System.Media.SoundPlayer olhoDireito = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/OlhoDireito.wav");
                olhoDireito.Play();
            }
        }
        private void seleOlhoEsquerdo_Click(object sender, RoutedEventArgs e)
        {
            HandCursor.Fill = new System.Windows.Media.ImageBrush(new System.Windows.Media.Imaging.BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Mao/Mao Olho Esquerdo.png", UriKind.Relative)));
            if (_objetoOlhoEsquerdoClicado == true)
            {
                seleOlhoEsquerdo.IsEnabled = false;
            }
            else
            {
                _olhoEsquerdoClicado = true;
                seleOlhoEsquerdo.Visibility = System.Windows.Visibility.Hidden;
                souOlhoEsquerdo.Visibility = System.Windows.Visibility.Hidden;
                if (_olhoEsquerdoClicado == true)
                {
                    _olhoDireitoClicado = false;
                    _bocaClicada = false;
                    _narizClicado = false;
                    if (_objetoOlhoDireitoClicado == false)
                    {
                        seleOlhoDireito.Visibility = System.Windows.Visibility.Visible;
                        souOlhoDireito.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoOlhoDireitoClicado == true)
                    {
                        seleOlhoDireito.IsEnabled = false;
                        seleOlhoDireito.Visibility = System.Windows.Visibility.Hidden;
                        souOlhoDireito.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoBocaClicada == false)
                    {
                        seleBoca.Visibility = System.Windows.Visibility.Visible;
                        souBoca.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoBocaClicada == true)
                    {
                        seleBoca.IsEnabled = false;
                        seleBoca.Visibility = System.Windows.Visibility.Hidden;
                        souBoca.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoNarizClicado == false)
                    {
                        seleNariz.Visibility = System.Windows.Visibility.Visible;
                        souNariz.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoNarizClicado == true)
                    {
                        seleNariz.IsEnabled = false;
                        seleNariz.Visibility = System.Windows.Visibility.Hidden;
                        souNariz.Visibility = System.Windows.Visibility.Visible;
                    }
                }

                System.Media.SoundPlayer olhoEsquerdo = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/OlhoEsquerdo.wav");
                olhoEsquerdo.Play();
            }
        }

        private void seleNariz_Click(object sender, RoutedEventArgs e)
        {
            HandCursor.Fill = new System.Windows.Media.ImageBrush(new System.Windows.Media.Imaging.BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Mao/Mao Nariz.png", UriKind.Relative)));
            if (_objetoNarizClicado == true)
            {
                seleNariz.IsEnabled = false;
            }
            else
            {
                _narizClicado = true;
                seleNariz.Visibility = System.Windows.Visibility.Hidden;
                souNariz.Visibility = System.Windows.Visibility.Hidden;
                if (_narizClicado == true)
                {
                    _olhoDireitoClicado = false;
                    _bocaClicada = false;
                    _olhoEsquerdoClicado = false;

                    if (_objetoOlhoDireitoClicado == false)
                    {
                        seleOlhoDireito.Visibility = System.Windows.Visibility.Visible;
                        souOlhoDireito.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoOlhoDireitoClicado == true)
                    {
                        seleOlhoDireito.IsEnabled = false;
                        seleOlhoDireito.Visibility = System.Windows.Visibility.Hidden;
                        souOlhoDireito.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoBocaClicada == false)
                    {
                        seleBoca.Visibility = System.Windows.Visibility.Visible;
                        souBoca.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoBocaClicada == true)
                    {
                        seleBoca.IsEnabled = false;
                        seleBoca.Visibility = System.Windows.Visibility.Hidden;
                        souBoca.Visibility = System.Windows.Visibility.Visible;
                    }
                }

                System.Media.SoundPlayer nariz = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/Nariz.wav");
                nariz.Play();
            }
        }

        private void soltarBoca_Click(object sender, RoutedEventArgs e)
        {
            if (_bocaClicada)
            {
                _objetoBocaClicada = true;
                Canvas.SetLeft(souBoca, 987);
                Canvas.SetTop(souBoca, 630);
                souBoca.Visibility = System.Windows.Visibility.Visible;
                parabens.Play();
                contaPontos.Acerto();
                _bocaClicada = false;
            }
            if (_olhoDireitoClicado)
            {
                _objetoOlhoDireitoClicado = true;
                Canvas.SetLeft(souOlhoDireito, 987);
                Canvas.SetTop(souOlhoDireito, 630);
                souOlhoDireito.Visibility = System.Windows.Visibility.Visible;
                naoFoiDessaVez.Play();
                contaPontos.Erro();
                _olhoDireitoClicado = false;
            }
            if (_olhoEsquerdoClicado)
            {
                _objetoOlhoEsquerdoClicado = true;
                Canvas.SetLeft(souOlhoEsquerdo, 987);
                Canvas.SetTop(souOlhoEsquerdo, 630);
                souOlhoEsquerdo.Visibility = System.Windows.Visibility.Visible;
                 naoFoiDessaVez.Play();
                contaPontos.Erro();
                _olhoEsquerdoClicado = false;
            }
            if (_narizClicado)
            {
                _objetoNarizClicado = true;
                Canvas.SetLeft(souNariz, 987);
                Canvas.SetTop(souNariz, 630);
                souNariz.Visibility = System.Windows.Visibility.Visible;
                naoFoiDessaVez.Play();
                contaPontos.Erro();
                _narizClicado = false;
            }
            txtPonto.Visibility = System.Windows.Visibility.Visible;
           txtPonto1.Visibility = System.Windows.Visibility.Visible;
           txtPonto1.Text = Convert.ToString(contaPontos.SomaPontos);
            if (_objetoBocaClicada   && _objetoOlhoDireitoClicado &&_objetoOlhoEsquerdoClicado&&_objetoNarizClicado)
            {
                DispatcherTimer dt = new DispatcherTimer();
                dt.Interval = TimeSpan.FromSeconds(1);
                dt.Tick += Dt_Tick;
                dt.Start();
            }
        }
        private void soltarOlhoDireito_Click(object sender, RoutedEventArgs e)
        {
            if (_bocaClicada)
            {
                _objetoBocaClicada = true;
                Canvas.SetLeft(souBoca, 824);
                Canvas.SetTop(souBoca, 285);
                souBoca.Visibility = System.Windows.Visibility.Visible;
                 naoFoiDessaVez.Play();
                contaPontos.Erro();
                _bocaClicada = false;
            }
           if (_olhoDireitoClicado)
            {
                _objetoOlhoDireitoClicado = true;
                Canvas.SetLeft(souOlhoDireito, 822);
                Canvas.SetTop(souOlhoDireito, 285);
                souOlhoDireito.Visibility = System.Windows.Visibility.Visible;
                parabens.Play();
                contaPontos.Acerto();
                _olhoDireitoClicado = false;
            }
            if (_olhoEsquerdoClicado)
            {
                _objetoOlhoEsquerdoClicado = true;
                Canvas.SetLeft(souOlhoEsquerdo, 824);
                Canvas.SetTop(souOlhoEsquerdo, 285);
                souOlhoEsquerdo.Visibility = System.Windows.Visibility.Visible;
                 naoFoiDessaVez.Play();
                contaPontos.Erro();
                _olhoEsquerdoClicado = false;
            }
            if (_narizClicado)
            {
                _objetoNarizClicado = true;
                Canvas.SetLeft(souNariz, 824);
                Canvas.SetTop(souNariz, 285);
                souNariz.Visibility = System.Windows.Visibility.Visible;
                naoFoiDessaVez.Play();
                contaPontos.Erro();
                _narizClicado = false;
            }
            txtPonto.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Text = Convert.ToString(contaPontos.SomaPontos);
            if (_objetoBocaClicada && _objetoOlhoDireitoClicado && _objetoOlhoEsquerdoClicado && _objetoNarizClicado)
            {
                DispatcherTimer dt = new DispatcherTimer();
                dt.Interval = TimeSpan.FromSeconds(1);
                dt.Tick += Dt_Tick;
                dt.Start();
            }
        }
        private void soltarOlhoEsquerdo_Click(object sender, RoutedEventArgs e)
        {
            if (_bocaClicada)
            {
                _objetoBocaClicada = true;
                Canvas.SetLeft(souBoca, 1122);
                Canvas.SetTop(souBoca, 285);
                souBoca.Visibility = System.Windows.Visibility.Visible;
                    naoFoiDessaVez.Play();
                contaPontos.Erro();
                _bocaClicada = false;
            }
            if (_olhoDireitoClicado)
            {
                _objetoOlhoDireitoClicado = true;
                Canvas.SetLeft(souOlhoDireito, 1122);
                Canvas.SetTop(souOlhoDireito, 285);
                souOlhoDireito.Visibility = System.Windows.Visibility.Visible;
                naoFoiDessaVez.Play();
                contaPontos.Erro();
                _olhoDireitoClicado = false;
            }
            if (_olhoEsquerdoClicado)
            {
                _objetoOlhoEsquerdoClicado = true;
                Canvas.SetLeft(souOlhoEsquerdo, 1120);
                Canvas.SetTop(souOlhoEsquerdo, 285);
                souOlhoEsquerdo.Visibility = System.Windows.Visibility.Visible;
                parabens.Play();
                contaPontos.Acerto();
                _olhoEsquerdoClicado = false;
            }
            if (_narizClicado)
            {
                _objetoNarizClicado = true;
                Canvas.SetLeft(souNariz, 1122);
                Canvas.SetTop(souNariz, 285);
                souNariz.Visibility = System.Windows.Visibility.Visible;
                naoFoiDessaVez.Play();
                contaPontos.Erro();
                _narizClicado = false;
            }
            txtPonto.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Text = Convert.ToString(contaPontos.SomaPontos);
            if (_objetoBocaClicada && _objetoOlhoDireitoClicado && _objetoOlhoEsquerdoClicado && _objetoNarizClicado)
            {
                DispatcherTimer dt = new DispatcherTimer();
                dt.Interval = TimeSpan.FromSeconds(1);
                dt.Tick += Dt_Tick;
                dt.Start();
            }
        }
        private void soltarNariz_Click(object sender, RoutedEventArgs e)
        {
            if (_narizClicado)
            {
                _objetoNarizClicado = true;
                Canvas.SetLeft(souNariz, 986);
                Canvas.SetTop(souNariz, 443);
                souNariz.Visibility = System.Windows.Visibility.Visible;
                parabens.Play();
                contaPontos.Acerto();
                _narizClicado = false;
            }


            if (_olhoDireitoClicado)
            {
                _objetoOlhoDireitoClicado = true;
                Canvas.SetLeft(souOlhoDireito, 986);
                Canvas.SetTop(souOlhoDireito, 443);
                souOlhoDireito.Visibility = System.Windows.Visibility.Visible;
                naoFoiDessaVez.Play();
                contaPontos.Erro();
                _olhoDireitoClicado = false;
            }
            if (_olhoEsquerdoClicado)
            {
                _objetoOlhoEsquerdoClicado = true;
                Canvas.SetLeft(souOlhoEsquerdo, 986);
                Canvas.SetTop(souOlhoEsquerdo, 443);
                souOlhoEsquerdo.Visibility = System.Windows.Visibility.Visible;
                naoFoiDessaVez.Play();
                contaPontos.Erro();
                _olhoEsquerdoClicado = false;
            }
            if (_bocaClicada)
            {
                _objetoBocaClicada = true;
                Canvas.SetLeft(souBoca, 986);
                Canvas.SetTop(souBoca, 443);
                souBoca.Visibility = System.Windows.Visibility.Visible;
                naoFoiDessaVez.Play();
                contaPontos.Erro();
                _bocaClicada = false;
            }
            txtPonto.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Text = Convert.ToString(contaPontos.SomaPontos);
            if (_objetoBocaClicada && _objetoOlhoDireitoClicado && _objetoOlhoEsquerdoClicado && _objetoNarizClicado)
            {
                DispatcherTimer dt = new DispatcherTimer();
                dt.Interval = TimeSpan.FromSeconds(1);
                dt.Tick += Dt_Tick;
                dt.Start();
            }
        }
        #endregion Eventos



    }
}
