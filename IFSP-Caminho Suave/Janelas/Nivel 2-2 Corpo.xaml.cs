﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
//adicionados as seguintes bibliotecas:
using Coding4Fun.Kinect.Wpf.Controls;
using System.Windows.Threading;
using Microsoft.Kinect;

namespace IFSP_Caminho_Suave.Janelas
{
    /// <summary>
    /// Interaction logic for Nivel_2_2_Corpo.xaml
    /// </summary>
    public partial class Nivel_2_2_Corpo : Window
    {
        #region Atributos
        private bool _cabecaClicada = false;
        private bool _bracoDireitoClicado = false;
        private bool _bracoEsquerdoClicado = false;
        private bool _pernaDireitaClicada = false;
        private bool _pernaEsquerdaClicada = false;
        private bool _objetoCabecaClicada = false;
        private bool _objetoBracoDireitoClicado = false;
        private bool _objetoBracoEsquerdoClicado = false;
        private bool _objetoPernaDireitaClicada = false;
        private bool _objetoPernaEsquerdaClicada = false;
        private static int larguraEcrã = 1500;
        private static int alturaTela = 1000;
        private KinectSensor _sensor;
        private Skeleton[] todosEsqueletos = new Skeleton[6];
        private List<HoverButton> botoesKinect;
        #endregion Atributos

        #region Janela principal
        public Nivel_2_2_Corpo()
        {
            InitializeComponent();
            botoesKinect = new List<HoverButton>()
            {
                soltarCabeca,
                soltarBracoEsquerdo,
                soltarBracoDireito,
                soltarPernaDireita,
                soltarPernaEsquerda,
                seleCabeca,
                seleBracoDireito,
                seleBracoEsquerdo,
                selePernaDireita,
                selePernaEsquerda
            };
        }
        #endregion Janela principal

        #region Captura
        void _sensor_AllFramesReady(object sender, AllFramesReadyEventArgs e)
        {
            Skeleton first = metdodos.GetFirstSkeleton(e);
            if (first == null)
            {
                return;
            }
            metdodos.ScalePosition(HandCursor, first.Joints[JointType.HandRight]);
            ProcessGesture(first.Joints[JointType.HandRight]);
            GetCameraPoint(first, e);
        }
        Classes.MetodosEPropriedades metdodos = new Classes.MetodosEPropriedades();
        void GetCameraPoint(Skeleton first, AllFramesReadyEventArgs e)
        {
            using (DepthImageFrame depth = e.OpenDepthImageFrame())
            {
                if (depth == null || _sensor == null)
                {
                    return;
                }
                DepthImagePoint rightDepthPoint =
                    depth.MapFromSkeletonPoint(first.Joints[JointType.HandRight].Position);
                ColorImagePoint rightColorPoint =
                    depth.MapToColorImagePoint(rightDepthPoint.X * 6, rightDepthPoint.Y * 3,
                        ColorImageFormat.RgbResolution640x480Fps30);
                metdodos.CameraPosition(HandCursor, rightColorPoint);
                foreach (HoverButton hb in botoesKinect)
                {
                    metdodos.CheckButton(hb, HandCursor);
                }
            }
        }
        #endregion Captura

 /*       #region Tempo
        private int incremento;
        private void Dt_Tick(object sender, EventArgs e)
        {
            incremento++;


            if (incremento == 3)
            {
                Nivel_2_1 n21 = new Nivel_2_1();
                n21.ShowDialog();
            }
        }
        #endregion Tempo
        */
        #region Processamentos
        private void ProcessGesture(Joint rightHand)
        {
            SkeletonPoint point = new SkeletonPoint();
            point.X = metdodos.ScaleVector(larguraEcrã, rightHand.Position.X);
            point.Y = metdodos.ScaleVector(alturaTela, -rightHand.Position.Y);
            point.Z = rightHand.Position.Z;
            rightHand.Position = point;
            if (_cabecaClicada && !_objetoCabecaClicada)
            {
                Canvas.SetLeft(souCabeca, rightHand.Position.X / 1);
                Canvas.SetTop(souCabeca, rightHand.Position.Y / 1.9);
            }
            if (_bracoDireitoClicado && !_objetoBracoDireitoClicado)
            {
                Canvas.SetLeft(souBracoDireito, rightHand.Position.X / 1);
                Canvas.SetTop(souBracoDireito, rightHand.Position.Y / 1.9);
            }
            if (_bracoEsquerdoClicado && !_objetoBracoEsquerdoClicado)
            {
                Canvas.SetLeft(souBracoEsquerdo, rightHand.Position.X / 1);
                Canvas.SetTop(souBracoEsquerdo, rightHand.Position.Y / 1.9);
            }
            if (_pernaDireitaClicada && !_objetoPernaDireitaClicada)
            {
                Canvas.SetLeft(souPernaDireita, rightHand.Position.X / 1);
                Canvas.SetTop(souPernaDireita, rightHand.Position.Y / 1.9);
            }
            if (_pernaEsquerdaClicada && !_objetoPernaEsquerdaClicada)
            {
                Canvas.SetLeft(souPernaEsquerda, rightHand.Position.X / 1);
                Canvas.SetTop(souPernaEsquerda, rightHand.Position.Y / 1.9);
            }
            if (rightHand.Position.Z > 0.70)
            {
                txtAviso.Visibility = Visibility.Visible;
                txtAviso.Text = "Venha mais para frente";
            }
        }
        #endregion Processamentos

        #region Carregar e fechar janelas 
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Top = 0;
            this.Left = 0;
            //  this.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            txtPonto.Visibility = System.Windows.Visibility.Hidden;
            txtPonto1.Visibility = System.Windows.Visibility.Hidden;
            souCabeca.Visibility = System.Windows.Visibility.Hidden;
            souBracoDireito.Visibility = System.Windows.Visibility.Hidden;
            souBracoEsquerdo.Visibility = System.Windows.Visibility.Hidden;
            souPernaDireita.Visibility = System.Windows.Visibility.Hidden;
            souPernaEsquerda.Visibility = System.Windows.Visibility.Hidden;
            if (KinectSensor.KinectSensors.Count > 0)
            {
                txtAviso.Visibility = Visibility.Hidden;
                _sensor = KinectSensor.KinectSensors[0];
                if (_sensor.Status == KinectStatus.Connected)
                {
                    _sensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
                    _sensor.AllFramesReady += new EventHandler<AllFramesReadyEventArgs>(_sensor_AllFramesReady);
                    var parameters = new TransformSmoothParameters
                    {
                        Smoothing = 0.3f,
                        Correction = 0.0f,
                        Prediction = 0.0f,
                        JitterRadius = 1.0f,
                        MaxDeviationRadius = 0.5f
                    };
                    _sensor.SkeletonStream.Enable(parameters);
                    _sensor.Start();
                    System.Media.SoundPlayer estruRosto = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/EstruRosto.wav");
                    estruRosto.Play();
                }
            }
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            if (_sensor != null)
            {
                _sensor.Stop();
            }
        }

        #endregion Carregar e fechar janelas 

        #region Eventos
        System.Media.SoundPlayer parabens = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/Parabens.wav");
        System.Media.SoundPlayer naoFoiDessaVez = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/NaoFoiDessaVez.wav");
        Classes.ContaPontos contaPontos = new Classes.ContaPontos();

        private void seleCabeca_Click(object sender, RoutedEventArgs e)
        {
        HandCursor.Fill = new System.Windows.Media.ImageBrush(new System.Windows.Media.Imaging.BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Mao/MaoCabeca.png", UriKind.Relative)));
            if (_objetoCabecaClicada == true)
            {
                seleCabeca.IsEnabled = false;
            }
            else
            {
                _cabecaClicada = true;
                seleCabeca.Visibility = System.Windows.Visibility.Hidden;
                souCabeca.Visibility = System.Windows.Visibility.Hidden;
                if (_cabecaClicada == true)
                {
                    _bracoDireitoClicado = false;
                    _bracoEsquerdoClicado = false;
                    _pernaDireitaClicada = false;
                    _pernaEsquerdaClicada = false;

                    if (_objetoBracoDireitoClicado == false)
                    {
                        seleBracoDireito.Visibility = System.Windows.Visibility.Visible;
                        souBracoDireito.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoBracoDireitoClicado == true)
                    {
                        seleBracoDireito.IsEnabled = false;
                        seleBracoDireito.Visibility = System.Windows.Visibility.Hidden;
                        souBracoDireito.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoBracoEsquerdoClicado == false)
                    {
                        seleBracoEsquerdo.Visibility = System.Windows.Visibility.Visible;
                        souBracoEsquerdo.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoBracoEsquerdoClicado == true)
                    {
                        seleBracoEsquerdo.IsEnabled = false;
                        seleBracoEsquerdo.Visibility = System.Windows.Visibility.Hidden;
                        souBracoEsquerdo.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoPernaDireitaClicada == false)
                    {
                        selePernaDireita.Visibility = System.Windows.Visibility.Visible;
                        souPernaDireita.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoPernaDireitaClicada == true)
                    {
                        selePernaDireita.IsEnabled = false;
                        selePernaDireita.Visibility = System.Windows.Visibility.Hidden;
                        souPernaDireita.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoPernaEsquerdaClicada == false)
                    {
                        selePernaEsquerda.Visibility = System.Windows.Visibility.Visible;
                        souPernaEsquerda.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoPernaDireitaClicada == true)
                    {
                        selePernaEsquerda.IsEnabled = false;
                        selePernaEsquerda.Visibility = System.Windows.Visibility.Hidden;
                        souPernaEsquerda.Visibility = System.Windows.Visibility.Visible;
                    }
                }

                ///////   System.Media.SoundPlayer cabeca = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/Boca.wav");
                //////cabeca.Play();
            }
        }

        private void seleBracoDireito_Click(object sender, RoutedEventArgs e)
        {
             HandCursor.Fill = new System.Windows.Media.ImageBrush(new System.Windows.Media.Imaging.BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Mao/MaoBracoDireito.png", UriKind.Relative)));
            if (_objetoBracoDireitoClicado == true)
            {
                seleBracoDireito.IsEnabled = false;
            }
            else
            {
                _bracoDireitoClicado = true;
                seleBracoDireito.Visibility = System.Windows.Visibility.Hidden;
                souBracoDireito.Visibility = System.Windows.Visibility.Hidden;
                if (_bracoDireitoClicado == true)
                {
                    _cabecaClicada = false;
                    _bracoEsquerdoClicado = false;
                    _pernaDireitaClicada = false;
                    _pernaEsquerdaClicada = false;

                    if (_objetoCabecaClicada == false)
                    {
                        seleCabeca.Visibility = System.Windows.Visibility.Visible;
                        souCabeca.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoCabecaClicada == true)
                    {
                        seleCabeca.IsEnabled = false;
                        seleCabeca.Visibility = System.Windows.Visibility.Hidden;
                        souCabeca.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoBracoEsquerdoClicado == false)
                    {
                        seleBracoEsquerdo.Visibility = System.Windows.Visibility.Visible;
                        souBracoEsquerdo.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoBracoEsquerdoClicado == true)
                    {
                        seleBracoEsquerdo.IsEnabled = false;
                        seleBracoEsquerdo.Visibility = System.Windows.Visibility.Hidden;
                        souBracoEsquerdo.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoPernaDireitaClicada == false)
                    {
                        selePernaDireita.Visibility = System.Windows.Visibility.Visible;
                        souPernaDireita.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoPernaDireitaClicada == true)
                    {
                        selePernaDireita.IsEnabled = false;
                        selePernaDireita.Visibility = System.Windows.Visibility.Hidden;
                        souPernaDireita.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoPernaEsquerdaClicada == false)
                    {
                        selePernaEsquerda.Visibility = System.Windows.Visibility.Visible;
                        souPernaEsquerda.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoPernaDireitaClicada == true)
                    {
                        selePernaEsquerda.IsEnabled = false;
                        selePernaEsquerda.Visibility = System.Windows.Visibility.Hidden;
                        souPernaEsquerda.Visibility = System.Windows.Visibility.Visible;
                    }
                }

                ///////   System.Media.SoundPlayer bracoDireito = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/Boca.wav");
                //////bracoDireito.Play();
            }
        }

        private void seleBracoEsquerdo_Click(object sender, RoutedEventArgs e)
        {
            HandCursor.Fill = new System.Windows.Media.ImageBrush(new System.Windows.Media.Imaging.BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Mao/MaoBracoEsquerdo.png", UriKind.Relative)));
            if (_objetoBracoEsquerdoClicado == true)
            {
                seleBracoEsquerdo.IsEnabled = false;
            }
            else
            {
                _bracoEsquerdoClicado = true;
                seleBracoEsquerdo.Visibility = System.Windows.Visibility.Hidden;
                souBracoEsquerdo.Visibility = System.Windows.Visibility.Hidden;
                if (_bracoEsquerdoClicado == true)
                {
                    _bracoDireitoClicado = false;
                    _cabecaClicada = false;
                    _pernaDireitaClicada = false;
                    _pernaEsquerdaClicada = false;

                    if (_objetoBracoDireitoClicado == false)
                    {
                        seleBracoDireito.Visibility = System.Windows.Visibility.Visible;
                        souBracoDireito.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoBracoDireitoClicado == true)
                    {
                        seleBracoDireito.IsEnabled = false;
                        seleBracoDireito.Visibility = System.Windows.Visibility.Hidden;
                        souBracoDireito.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoCabecaClicada == false)
                    {
                        seleCabeca.Visibility = System.Windows.Visibility.Visible;
                        souCabeca.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoCabecaClicada == true)
                    {
                        seleCabeca.IsEnabled = false;
                        seleCabeca.Visibility = System.Windows.Visibility.Hidden;
                        souCabeca.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoPernaDireitaClicada == false)
                    {
                        selePernaDireita.Visibility = System.Windows.Visibility.Visible;
                        souPernaDireita.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoPernaDireitaClicada == true)
                    {
                        selePernaDireita.IsEnabled = false;
                        selePernaDireita.Visibility = System.Windows.Visibility.Hidden;
                        souPernaDireita.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoPernaEsquerdaClicada == false)
                    {
                        selePernaEsquerda.Visibility = System.Windows.Visibility.Visible;
                        souPernaEsquerda.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoPernaDireitaClicada == true)
                    {
                        selePernaEsquerda.IsEnabled = false;
                        selePernaEsquerda.Visibility = System.Windows.Visibility.Hidden;
                        souPernaEsquerda.Visibility = System.Windows.Visibility.Visible;
                    }
                }

                ///////   System.Media.SoundPlayer bracoEsquerdo = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/Boca.wav");
                //////bracoEsquerdo.Play();
            }
        }
        private void selePernaDireita_Click(object sender, RoutedEventArgs e)
        {
              HandCursor.Fill = new System.Windows.Media.ImageBrush(new System.Windows.Media.Imaging.BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Mao/MaoPernaDireita.png", UriKind.Relative)));
            if (_objetoPernaDireitaClicada == true)
            {
                selePernaDireita.IsEnabled = false;
            }
            else
            {
                _pernaDireitaClicada = true;
                selePernaDireita.Visibility = System.Windows.Visibility.Hidden;
                souPernaDireita.Visibility = System.Windows.Visibility.Hidden;
                if (_pernaDireitaClicada == true)
                {
                    _bracoDireitoClicado = false;
                    _bracoEsquerdoClicado = false;
                    _cabecaClicada = false;
                    _pernaEsquerdaClicada = false;

                    if (_objetoBracoDireitoClicado == false)
                    {
                        seleBracoDireito.Visibility = System.Windows.Visibility.Visible;
                        souBracoDireito.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoBracoDireitoClicado == true)
                    {
                        seleBracoDireito.IsEnabled = false;
                        seleBracoDireito.Visibility = System.Windows.Visibility.Hidden;
                        souBracoDireito.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoBracoEsquerdoClicado == false)
                    {
                        seleBracoEsquerdo.Visibility = System.Windows.Visibility.Visible;
                        souBracoEsquerdo.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoBracoEsquerdoClicado == true)
                    {
                        seleBracoEsquerdo.IsEnabled = false;
                        seleBracoEsquerdo.Visibility = System.Windows.Visibility.Hidden;
                        souBracoEsquerdo.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoCabecaClicada == false)
                    {
                        seleCabeca.Visibility = System.Windows.Visibility.Visible;
                        souCabeca.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoCabecaClicada == true)
                    {
                        seleCabeca.IsEnabled = false;
                        seleCabeca.Visibility = System.Windows.Visibility.Hidden;
                        souCabeca.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoPernaEsquerdaClicada == false)
                    {
                        selePernaEsquerda.Visibility = System.Windows.Visibility.Visible;
                        souPernaEsquerda.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoPernaDireitaClicada == true)
                    {
                        selePernaEsquerda.IsEnabled = false;
                        selePernaEsquerda.Visibility = System.Windows.Visibility.Hidden;
                        souPernaEsquerda.Visibility = System.Windows.Visibility.Visible;
                    }
                }

                ///////   System.Media.SoundPlayer pernaDireita = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/Boca.wav");
                //////pernaDireita.Play();
            }
        }

        private void selePernaEsquerda_Click(object sender, RoutedEventArgs e)
        {
             HandCursor.Fill = new System.Windows.Media.ImageBrush(new System.Windows.Media.Imaging.BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Mao/MaoPernaEsquerda.png", UriKind.Relative)));
            if (_objetoPernaEsquerdaClicada == true)
            {
                selePernaEsquerda.IsEnabled = false;
            }
            else
            {
                _pernaEsquerdaClicada = true;
                selePernaEsquerda.Visibility = System.Windows.Visibility.Hidden;
                souPernaEsquerda.Visibility = System.Windows.Visibility.Hidden;
                if (_pernaEsquerdaClicada == true)
                {
                    _bracoDireitoClicado = false;
                    _bracoEsquerdoClicado = false;
                    _pernaDireitaClicada = false;
                    _cabecaClicada = false;

                    if (_objetoBracoDireitoClicado == false)
                    {
                        seleBracoDireito.Visibility = System.Windows.Visibility.Visible;
                        souBracoDireito.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoBracoDireitoClicado == true)
                    {
                        seleBracoDireito.IsEnabled = false;
                        seleBracoDireito.Visibility = System.Windows.Visibility.Hidden;
                        souBracoDireito.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoBracoEsquerdoClicado == false)
                    {
                        seleBracoEsquerdo.Visibility = System.Windows.Visibility.Visible;
                        souBracoEsquerdo.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoBracoEsquerdoClicado == true)
                    {
                        seleBracoEsquerdo.IsEnabled = false;
                        seleBracoEsquerdo.Visibility = System.Windows.Visibility.Hidden;
                        souBracoEsquerdo.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoPernaDireitaClicada == false)
                    {
                        selePernaDireita.Visibility = System.Windows.Visibility.Visible;
                        souPernaDireita.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoPernaDireitaClicada == true)
                    {
                        selePernaDireita.IsEnabled = false;
                        selePernaDireita.Visibility = System.Windows.Visibility.Hidden;
                        souPernaDireita.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoCabecaClicada == false)
                    {
                        seleCabeca.Visibility = System.Windows.Visibility.Visible;
                        souCabeca.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoCabecaClicada == true)
                    {
                        seleCabeca.IsEnabled = false;
                        seleCabeca.Visibility = System.Windows.Visibility.Hidden;
                        souCabeca.Visibility = System.Windows.Visibility.Visible;
                    }
                }

                ///////   System.Media.SoundPlayer pernaEsquerda = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/Boca.wav");
                //////pernaEsquerda.Play();
            }
        }


        private void soltarCabeca_Click(object sender, RoutedEventArgs e)
        {
            if (_cabecaClicada)
            {
                _objetoCabecaClicada = true;
                Canvas.SetLeft(souCabeca, 975);
                Canvas.SetTop(souCabeca, 24);
                souCabeca.Visibility = System.Windows.Visibility.Visible;
                parabens.Play();
                contaPontos.Acerto();
                _cabecaClicada = false;
                soltarCabeca.Visibility = System.Windows.Visibility.Hidden;
            }
           
            txtPonto.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Text = Convert.ToString(contaPontos.SomaPontos);
        /*    if (_objetoCabecaClicada && _objetoBracoDireitoClicado && _objetoBracoEsquerdoClicado && _objetoPernaDireitaClicada && _objetoPernaEsquerdaClicada)
            {
                DispatcherTimer dt = new DispatcherTimer();
                dt.Interval = TimeSpan.FromSeconds(1);
                dt.Tick += Dt_Tick;
                dt.Start();
            }*/
        }

        private void soltarBracoDireito_Click(object sender, RoutedEventArgs e)
        {
            if (_bracoDireitoClicado)
            {
                _objetoBracoDireitoClicado = true;
                Canvas.SetLeft(souBracoDireito, 728);
                Canvas.SetTop(souBracoDireito, 127);
                souBracoDireito.Visibility = System.Windows.Visibility.Visible;
                parabens.Play();
                contaPontos.Acerto();
                _bracoDireitoClicado = false;
                soltarBracoDireito.Visibility = System.Windows.Visibility.Hidden;
            }
         
            txtPonto.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Text = Convert.ToString(contaPontos.SomaPontos);
            /*      if (_objetoCabecaClicada && _objetoBracoDireitoClicado && _objetoBracoEsquerdoClicado && _objetoPernaDireitaClicada && _objetoPernaEsquerdaClicada)
                {
                     DispatcherTimer dt = new DispatcherTimer();
                     dt.Interval = TimeSpan.FromSeconds(1);
                     dt.Tick += Dt_Tick;
                     dt.Start();
                 }*/
        }
        private void soltarBracoEsquerdo_Click(object sender, RoutedEventArgs e)
        {
           if (_bracoEsquerdoClicado)
            {
                _objetoBracoEsquerdoClicado = true;
                Canvas.SetLeft(souBracoEsquerdo, 1185);
                Canvas.SetTop(souBracoEsquerdo, 155);
                souBracoEsquerdo.Visibility = System.Windows.Visibility.Visible;
                parabens.Play();
                contaPontos.Acerto();
                _bracoEsquerdoClicado = false;
                soltarBracoEsquerdo.Visibility = System.Windows.Visibility.Hidden;
            }
           txtPonto.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Text = Convert.ToString(contaPontos.SomaPontos);
         /*   if (_objetoCabecaClicada && _objetoBracoDireitoClicado && _objetoBracoEsquerdoClicado && _objetoPernaDireitaClicada && _objetoPernaEsquerdaClicada)
            {
                DispatcherTimer dt = new DispatcherTimer();
                dt.Interval = TimeSpan.FromSeconds(1);
                dt.Tick += Dt_Tick;
                dt.Start();
            }*/
         
        }
        private void soltarPernaDireita_Click(object sender, RoutedEventArgs e)
        {
            
            if (_pernaDireitaClicada)
            {
                _objetoPernaDireitaClicada = true;
                Canvas.SetLeft(souPernaDireita, 877);
                Canvas.SetTop(souPernaDireita, 599);
                souPernaDireita.Visibility = System.Windows.Visibility.Visible;
                parabens.Play();
                contaPontos.Acerto();
                _pernaDireitaClicada = false;
                soltarPernaDireita.Visibility = System.Windows.Visibility.Hidden;
            }
         
            txtPonto.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Text = Convert.ToString(contaPontos.SomaPontos);
           /* if (_objetoCabecaClicada && _objetoBracoDireitoClicado && _objetoBracoEsquerdoClicado && _objetoPernaDireitaClicada && _objetoPernaEsquerdaClicada)
            {
                DispatcherTimer dt = new DispatcherTimer();
                dt.Interval = TimeSpan.FromSeconds(1);
                dt.Tick += Dt_Tick;
                dt.Start();
            }*/
        }

        private void soltarPernaEsquerda_Click(object sender, RoutedEventArgs e)
        {
            
            if (_pernaEsquerdaClicada)
            {
                _objetoPernaEsquerdaClicada = true;
                Canvas.SetLeft(souPernaEsquerda, 1113);
                Canvas.SetTop(souPernaEsquerda, 601);
                souPernaEsquerda.Visibility = System.Windows.Visibility.Visible;
                parabens.Play();
                contaPontos.Acerto();
                _pernaEsquerdaClicada = false;
                soltarPernaEsquerda.Visibility = System.Windows.Visibility.Hidden;
            }
        
            txtPonto.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Text = Convert.ToString(contaPontos.SomaPontos);
          /*  if (_objetoCabecaClicada && _objetoBracoDireitoClicado && _objetoBracoEsquerdoClicado && _objetoPernaDireitaClicada && _objetoPernaEsquerdaClicada)
            {
                DispatcherTimer dt = new DispatcherTimer();
                dt.Interval = TimeSpan.FromSeconds(1);
                dt.Tick += Dt_Tick;
                dt.Start();
            }*/
        }

        #endregion Eventos

    }
}
