﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
//adicionados as seguintes bibliotecas:
using Coding4Fun.Kinect.Wpf.Controls;
using System.Windows.Threading;
using Microsoft.Kinect;

namespace IFSP_Caminho_Suave.Janelas
{
    /// <summary>
    /// Interaction logic for Nivel_2_1.xaml
    /// </summary>
    public partial class Nivel_2_1 : Window
    {
        #region Atributos
        private bool _narizClicado = false;
        private bool _orelhaDireitaClicada = false;
        private bool _orelhaEsquerdaClicada = false;
        private bool _objetoNarizClicado = false;
        private bool _objetoOrelhaDireitaClicada= false;
        private bool _objetoOrelhaEsquerdaClicada = false;
        private static int larguraEcrã = 1500;
        private static int alturaTela = 1000;
        private KinectSensor _sensor;
        private Skeleton[] todosEsqueletos = new Skeleton[6];
        private List<HoverButton> botoesKinect;
        #endregion Atributos

        #region Janela principal
        public Nivel_2_1()
        {
            InitializeComponent();
            botoesKinect = new List<HoverButton>()
            {
                soltarNariz,
                soltarOrelhaDireita,
                soltarOrelhaEsquerda,
                seleNariz,
                seleOrelhaDireita,
                seleOrelhaEsquerda
          };
        }
        #endregion Janela principal

        #region Captura
        void _sensor_AllFramesReady(object sender, AllFramesReadyEventArgs e)
        {
            Skeleton first = metdodos.GetFirstSkeleton(e);
            if (first == null)
            {
                return;
            }
            metdodos.ScalePosition(HandCursor, first.Joints[JointType.HandRight]);
            ProcessGesture(first.Joints[JointType.HandRight]);
            GetCameraPoint(first, e);
        }
        Classes.MetodosEPropriedades metdodos = new Classes.MetodosEPropriedades();
        void GetCameraPoint(Skeleton first, AllFramesReadyEventArgs e)
        {
            using (DepthImageFrame depth = e.OpenDepthImageFrame())
            {
                if (depth == null || _sensor == null)
                {
                    return;
                }
                DepthImagePoint rightDepthPoint =
                    depth.MapFromSkeletonPoint(first.Joints[JointType.HandRight].Position);
                ColorImagePoint rightColorPoint =
                    depth.MapToColorImagePoint(rightDepthPoint.X * 6, rightDepthPoint.Y * 3,
                        ColorImageFormat.RgbResolution640x480Fps30);
                metdodos.CameraPosition(HandCursor, rightColorPoint);
                foreach (HoverButton hb in botoesKinect)
                {
                    metdodos.CheckButton(hb, HandCursor);
                }
            }
        }
        #endregion Captura

        #region Tempo
        private int incremento;
        private void Dt_Tick(object sender, EventArgs e)
        {
            incremento++;


            if (incremento == 3)
            {
              //  Nivel_1_0 n10 = new Nivel_1_0();
             //   n10.ShowDialog();
            }
        }
        #endregion Tempo

        #region Processamentos
        private void ProcessGesture(Joint rightHand)
        {
            SkeletonPoint point = new SkeletonPoint();
            point.X = metdodos.ScaleVector(larguraEcrã, rightHand.Position.X);
            point.Y = metdodos.ScaleVector(alturaTela, -rightHand.Position.Y);
            point.Z = rightHand.Position.Z;
            rightHand.Position = point;
            if (_narizClicado && !_objetoNarizClicado)
            {
                Canvas.SetLeft(souNariz, rightHand.Position.X / 1);
                Canvas.SetTop(souNariz, rightHand.Position.Y / 1.9);
            }
            if (_orelhaDireitaClicada && !_objetoOrelhaDireitaClicada)
            {
                Canvas.SetLeft(souOrelhaDireita, rightHand.Position.X / 1);
                Canvas.SetTop(souOrelhaDireita, rightHand.Position.Y / 1.9);
            }
            if (_orelhaEsquerdaClicada && !_objetoOrelhaEsquerdaClicada)
            {
                Canvas.SetLeft(souOrelhaEsquerda, rightHand.Position.X / 1);
                Canvas.SetTop(souOrelhaEsquerda, rightHand.Position.Y / 1.9);
            }
            if (rightHand.Position.Z > 0.70)
            {
                txtAviso.Visibility = Visibility.Visible;
                txtAviso.Text = "Venha mais para frente";
            }
        }
        #endregion Processamentos

        #region Carregar e fechar janelas 
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Top = 0;
            this.Left = 0;
            //  this.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            txtPonto.Visibility = System.Windows.Visibility.Hidden;
            txtPonto1.Visibility = System.Windows.Visibility.Hidden;
            souNariz.Visibility = System.Windows.Visibility.Hidden;
            souOrelhaDireita.Visibility = System.Windows.Visibility.Hidden;
            souOrelhaEsquerda.Visibility = System.Windows.Visibility.Hidden;
            if (KinectSensor.KinectSensors.Count > 0)
            {
                txtAviso.Visibility = Visibility.Hidden;
                _sensor = KinectSensor.KinectSensors[0];
                if (_sensor.Status == KinectStatus.Connected)
                {
                    _sensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
                    _sensor.AllFramesReady += new EventHandler<AllFramesReadyEventArgs>(_sensor_AllFramesReady);
                    var parameters = new TransformSmoothParameters
                    {
                        Smoothing = 0.3f,
                        Correction = 0.0f,
                        Prediction = 0.0f,
                        JitterRadius = 1.0f,
                        MaxDeviationRadius = 0.5f
                    };
                    _sensor.SkeletonStream.Enable(parameters);
                    _sensor.Start();
                      System.Media.SoundPlayer estruRosto = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/EstruRosto.wav");
                    estruRosto.Play();
                }
            }
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            if (_sensor != null)
            {
                _sensor.Stop();
            }
        }

        #endregion Carregar e fechar janelas 

        #region Eventos
        System.Media.SoundPlayer parabens = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/Parabens.wav");
        System.Media.SoundPlayer naoFoiDessaVez = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/NaoFoiDessaVez.wav");
        Classes.ContaPontos contaPontos = new Classes.ContaPontos();
       
        private void seleNariz_Click(object sender, RoutedEventArgs e)
        {
            HandCursor.Fill = new System.Windows.Media.ImageBrush(new System.Windows.Media.Imaging.BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Mao/Mao Nariz.png", UriKind.Relative)));
            if (_objetoNarizClicado == true)
            {
                seleNariz.IsEnabled = false;
            }
            else
            {
                _narizClicado = true;
                seleNariz.Visibility = System.Windows.Visibility.Hidden;
                souNariz.Visibility = System.Windows.Visibility.Hidden;
                if (_narizClicado == true)
                {
                    _orelhaDireitaClicada = false;
                    _orelhaEsquerdaClicada = false;
                    if (_objetoOrelhaDireitaClicada == false)
                    {
                        seleOrelhaDireita.Visibility = System.Windows.Visibility.Visible;
                        souOrelhaDireita.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoOrelhaDireitaClicada == true)
                    {
                        seleOrelhaDireita.IsEnabled = false;
                        seleOrelhaDireita.Visibility = System.Windows.Visibility.Hidden;
                        souOrelhaDireita.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoOrelhaEsquerdaClicada == false)
                    {
                        seleOrelhaEsquerda.Visibility = System.Windows.Visibility.Visible;
                        souOrelhaEsquerda.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoOrelhaEsquerdaClicada == true)
                    {
                        seleOrelhaEsquerda.IsEnabled = false;
                        seleOrelhaEsquerda.Visibility = System.Windows.Visibility.Hidden;
                        souOrelhaEsquerda.Visibility = System.Windows.Visibility.Visible;
                    }
                }

                  System.Media.SoundPlayer nariz = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/Nariz.wav");
                nariz.Play();
            }
        }
        private void seleOrelhaDireita_Click(object sender, RoutedEventArgs e)
        {
            HandCursor.Fill = new System.Windows.Media.ImageBrush(new System.Windows.Media.Imaging.BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Mao/Mao Orelha Direita.png", UriKind.Relative)));
            if (_objetoOrelhaDireitaClicada == true)
            {
                seleOrelhaDireita.IsEnabled = false;
            }
            else
            {
                _orelhaDireitaClicada = true;
                seleOrelhaDireita.Visibility = System.Windows.Visibility.Hidden;
                souOrelhaDireita.Visibility = System.Windows.Visibility.Hidden;
                if (_orelhaDireitaClicada == true)
                {
                    _narizClicado = false;
                    _orelhaEsquerdaClicada = false;
                    if (_objetoNarizClicado == false)
                    {
                        seleNariz.Visibility = System.Windows.Visibility.Visible;
                        souNariz.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoNarizClicado == true)
                    {
                        seleNariz.IsEnabled = false;
                        seleNariz.Visibility = System.Windows.Visibility.Hidden;
                        souNariz.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoOrelhaEsquerdaClicada == false)
                    {
                        seleOrelhaEsquerda.Visibility = System.Windows.Visibility.Visible;
                        souOrelhaEsquerda.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoOrelhaEsquerdaClicada == true)
                    {
                        seleOrelhaEsquerda.IsEnabled = false;
                        seleOrelhaEsquerda.Visibility = System.Windows.Visibility.Hidden;
                        souOrelhaEsquerda.Visibility = System.Windows.Visibility.Visible;
                    }
                }

                 System.Media.SoundPlayer orelhaDireita = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/OrelhaDireita.wav");
                orelhaDireita.Play();
            }
        }
        private void seleOrelhaEsquerda_Click(object sender, RoutedEventArgs e)
        {
             HandCursor.Fill = new System.Windows.Media.ImageBrush(new System.Windows.Media.Imaging.BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Mao/Mao Orelha Esquerda.png", UriKind.Relative)));
            if (_objetoOrelhaEsquerdaClicada == true)
            {
                seleOrelhaEsquerda.IsEnabled = false;
            }
            else
            {
                _orelhaEsquerdaClicada = true;
                seleOrelhaEsquerda.Visibility = System.Windows.Visibility.Hidden;
                souOrelhaEsquerda.Visibility = System.Windows.Visibility.Hidden;
                if (_orelhaEsquerdaClicada == true)
                {
                    _orelhaDireitaClicada = false;
                    _narizClicado = false;
                    if (_objetoOrelhaDireitaClicada == false)
                    {
                        seleOrelhaDireita.Visibility = System.Windows.Visibility.Visible;
                        souOrelhaDireita.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoOrelhaDireitaClicada == true)
                    {
                        seleOrelhaDireita.IsEnabled = false;
                        seleOrelhaDireita.Visibility = System.Windows.Visibility.Hidden;
                        souOrelhaDireita.Visibility = System.Windows.Visibility.Visible;
                    }
                    if (_objetoNarizClicado == false)
                    {
                        seleNariz.Visibility = System.Windows.Visibility.Visible;
                        souNariz.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoNarizClicado == true)
                    {
                        seleNariz.IsEnabled = false;
                        seleNariz.Visibility = System.Windows.Visibility.Hidden;
                        souNariz.Visibility = System.Windows.Visibility.Visible;
                    }
                }

                 System.Media.SoundPlayer orelhaEsquerda = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/OrelhaEsquerda.wav");
                orelhaEsquerda.Play();
            }
        }
        private void soltarNariz_Click(object sender, RoutedEventArgs e)
        {
            if (_narizClicado)
            {
                _objetoNarizClicado = true;
                Canvas.SetLeft(souNariz, 1023);
                Canvas.SetTop(souNariz, 450);
                souNariz.Visibility = System.Windows.Visibility.Visible;
                 parabens.Play();
                contaPontos.Acerto();
                _narizClicado = false;
            }
            if (_orelhaDireitaClicada)
            {
                _objetoOrelhaDireitaClicada = true;
                Canvas.SetLeft(souOrelhaDireita, 1023);
                Canvas.SetTop(souOrelhaDireita, 450);
                souOrelhaDireita.Visibility = System.Windows.Visibility.Visible;
                naoFoiDessaVez.Play();
                contaPontos.Erro();
                _orelhaDireitaClicada = false;
            }
            if (_orelhaEsquerdaClicada)
            {
                _objetoOrelhaEsquerdaClicada = true;
                Canvas.SetLeft(souOrelhaEsquerda, 1023);
                Canvas.SetTop(souOrelhaEsquerda, 450);
                souOrelhaEsquerda.Visibility = System.Windows.Visibility.Visible;
                naoFoiDessaVez.Play();
                contaPontos.Erro();
                _orelhaEsquerdaClicada = false;
            }
            txtPonto.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Text = Convert.ToString(contaPontos.SomaPontos);
            if (_objetoNarizClicado && _objetoOrelhaDireitaClicada && _objetoOrelhaEsquerdaClicada)
            {
                DispatcherTimer dt = new DispatcherTimer();
                dt.Interval = TimeSpan.FromSeconds(1);
                dt.Tick += Dt_Tick;
                dt.Start();
            }
        }
        private void soltarOrelhaDireita_Click(object sender, RoutedEventArgs e)
        {
            if (_narizClicado)
            {
                _objetoNarizClicado = true;
                Canvas.SetLeft(souNariz, 745);
                Canvas.SetTop(souNariz, 292);
                souNariz.Visibility = System.Windows.Visibility.Visible;
                naoFoiDessaVez.Play();
                contaPontos.Erro();
                _narizClicado = false;
            }
            if (_orelhaDireitaClicada)
            {
                _objetoOrelhaDireitaClicada = true;
                Canvas.SetLeft(souOrelhaDireita, 745);
                Canvas.SetTop(souOrelhaDireita, 292);
                souOrelhaDireita.Visibility = System.Windows.Visibility.Visible;
                parabens.Play();
                contaPontos.Acerto();
                _orelhaDireitaClicada = false;
            }
            if (_orelhaEsquerdaClicada)
            {
                _objetoOrelhaEsquerdaClicada = true;
                Canvas.SetLeft(souOrelhaEsquerda, 745);
                Canvas.SetTop(souOrelhaEsquerda, 292);
                souOrelhaEsquerda.Visibility = System.Windows.Visibility.Visible;
                naoFoiDessaVez.Play();
                contaPontos.Erro();
                _orelhaEsquerdaClicada = false;
            }
            txtPonto.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Text = Convert.ToString(contaPontos.SomaPontos);
            if (_objetoNarizClicado && _objetoOrelhaDireitaClicada && _objetoOrelhaEsquerdaClicada)
            {
                DispatcherTimer dt = new DispatcherTimer();
                dt.Interval = TimeSpan.FromSeconds(1);
                dt.Tick += Dt_Tick;
                dt.Start();
            }
        }
        private void soltarOrelhaEsquerda_Click(object sender, RoutedEventArgs e)
        {
            if (_narizClicado)
            {
                _objetoNarizClicado = true;
                Canvas.SetLeft(souNariz, 1373);
                Canvas.SetTop(souNariz, 293);
                souNariz.Visibility = System.Windows.Visibility.Visible;
                naoFoiDessaVez.Play();
                contaPontos.Erro();
                _narizClicado = false;
            }
            if (_orelhaDireitaClicada)
            {
                _objetoOrelhaDireitaClicada = true;
                Canvas.SetLeft(souOrelhaDireita, 1373);
                Canvas.SetTop(souOrelhaDireita, 293);
                souOrelhaDireita.Visibility = System.Windows.Visibility.Visible;
                naoFoiDessaVez.Play();
                contaPontos.Erro();
                _orelhaDireitaClicada = false;
            }
            if (_orelhaEsquerdaClicada)
            {
                _objetoOrelhaEsquerdaClicada = true;
                Canvas.SetLeft(souOrelhaEsquerda, 1373);
                Canvas.SetTop(souOrelhaEsquerda, 293);
                souOrelhaEsquerda.Visibility = System.Windows.Visibility.Visible;
                parabens.Play();
                contaPontos.Acerto();
                _orelhaEsquerdaClicada = false;
            }
            txtPonto.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Text = Convert.ToString(contaPontos.SomaPontos);
            if (_objetoNarizClicado && _objetoOrelhaDireitaClicada && _objetoOrelhaEsquerdaClicada)
            {
                DispatcherTimer dt = new DispatcherTimer();
                dt.Interval = TimeSpan.FromSeconds(1);
                dt.Tick += Dt_Tick;
                dt.Start();
            }
        }
        #endregion Eventos

        
    }
}
