﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
//adicionados as seguintes bibliotecas:
using Coding4Fun.Kinect.Wpf.Controls;
using System.Windows.Threading;
using Microsoft.Kinect;

namespace IFSP_Caminho_Suave.Janelas
{
    /// <summary>
    /// Interaction logic for Nivel_1_0.xaml
    /// </summary>
    public partial class Nivel_1_0 : Window
    {       
        #region Atributos
        private bool _vermelhoClicado = false;
        private bool _azulClicado = false;
        private bool _amareloClicado = false;
        private bool _letraClicada = true;
        private bool _janelaAnterior = true;
        private bool _janelaPosterior = true;
        private bool _menuPrincipal = true;
        private bool _objetoVermelhoClicado = false;
        private bool _objetoAzulClicado = false;
        private bool _objetoAmareloClicado = false;
        private static int larguraEcrã = 1500;
        private static int alturaTela = 1000;
        private KinectSensor _sensor;
        private Skeleton[] todosEsqueletos = new Skeleton[6];
        private List<HoverButton> botoesKinect;
        #endregion Atributos

        #region Janela principal
        public Nivel_1_0()
        {
            InitializeComponent();
            botoesKinect = new List<HoverButton>()
            {
                soltarCor,
                seleAzul,
                seleAmarela,
                seleVermelho,
                Letra,
                Proximo,
                Anterior,
                MenuPrincipal
            };
            this.Closing += new System.ComponentModel.CancelEventHandler(Window_Closing);
        }

        #endregion Janela principal

        #region Captura
        void _sensor_AllFramesReady(object sender, AllFramesReadyEventArgs e)
        {      
            Skeleton first = metdodos.GetFirstSkeleton(e);
            if (first == null)
            {
                return;
            }
            metdodos.ScalePosition(HandCursor, first.Joints[JointType.HandRight]);
            ProcessGesture(first.Joints[JointType.HandRight]);
            GetCameraPoint(first, e);
        }
        Classes.MetodosEPropriedades metdodos = new Classes.MetodosEPropriedades();
        void GetCameraPoint(Skeleton first, AllFramesReadyEventArgs e)
        {
            using (DepthImageFrame depth = e.OpenDepthImageFrame())
            {
                if (depth == null || _sensor == null)
                {
                    return;
                }
                DepthImagePoint rightDepthPoint =
                    depth.MapFromSkeletonPoint(first.Joints[JointType.HandRight].Position);
                ColorImagePoint rightColorPoint =
                    depth.MapToColorImagePoint(rightDepthPoint.X * 6, rightDepthPoint.Y * 3,
                        ColorImageFormat.RgbResolution640x480Fps30);

                metdodos.CameraPosition(HandCursor, rightColorPoint);
                foreach (HoverButton hb in botoesKinect)
                {
                    metdodos.CheckButton(hb, HandCursor);
                }
            }
        }
        #endregion Captura

      /*  #region Tempo
        private int incremento;
        private void Dt_Tick(object sender, EventArgs e)
        {
            incremento++;
           
            
            if (incremento == 3)
            {
                Nivel_1_1 n11 = new Nivel_1_1();
                n11.ShowDialog();
            }
        }
        #endregion Tempo
*/
        #region Processamentos
        private void ProcessGesture(Joint rightHand)
        {
            SkeletonPoint point = new SkeletonPoint();
            point.X = metdodos.ScaleVector(larguraEcrã, rightHand.Position.X);
            point.Y = metdodos.ScaleVector(alturaTela, -rightHand.Position.Y  );
            point.Z = rightHand.Position.Z;
            rightHand.Position = point;
            if (_azulClicado && !_objetoAzulClicado)
            {
                Canvas.SetLeft(souLeAzul, rightHand.Position.X  );
                Canvas.SetTop(souLeAzul, rightHand.Position.Y );
            }
            if (_vermelhoClicado && !_objetoVermelhoClicado)
            {
                Canvas.SetLeft(souLeVermelho, rightHand.Position.X *6);
                Canvas.SetTop(souLeVermelho, rightHand.Position.Y *3);
            }
            if (_amareloClicado && !_objetoAmareloClicado)
            {
                Canvas.SetLeft(souLeAmarela, rightHand.Position.X *6);
                Canvas.SetTop(souLeAmarela, rightHand.Position.Y *3);
            }
            //profundidade
            //caso eu queira deixar ate determinada distancia 
            if (rightHand.Position.Z > 0.70)
            {
                txtAviso.Visibility = Visibility.Visible;
                txtAviso.Text = "Venha mais para frente";
            }
        }
        #endregion Processamentos

        #region Carregar e fechar janelas 
         private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
           this.Top = 0;
            this.Left = 0;
            txtPonto.Visibility = System.Windows.Visibility.Hidden;
            txtPonto1.Visibility = System.Windows.Visibility.Hidden;
            souLeAmarela.Visibility = System.Windows.Visibility.Hidden;
            souLeAzul.Visibility = System.Windows.Visibility.Hidden;
            souLeVermelho.Visibility = System.Windows.Visibility.Hidden;
            LetraAAzul.Visibility = System.Windows.Visibility.Hidden;
            LetraAAmarela.Visibility = System.Windows.Visibility.Hidden;
            LetraAVermelha.Visibility = System.Windows.Visibility.Hidden;
            if (KinectSensor.KinectSensors.Count > 0)
            {
                txtAviso.Visibility = Visibility.Hidden;
                _sensor = KinectSensor.KinectSensors[0];
                if (_sensor.Status == KinectStatus.Connected)
                {
                    _sensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
                    _sensor.AllFramesReady += new EventHandler<AllFramesReadyEventArgs>(_sensor_AllFramesReady);
                    var parameters = new TransformSmoothParameters
                    {
                        Smoothing = 0.3f,
                        Correction = 0.0f,
                        Prediction = 0.0f,
                        JitterRadius = 1.0f,
                        MaxDeviationRadius = 0.5f
                    };
                    _sensor.SkeletonStream.Enable(parameters);
                    _sensor.Start();
                     System.Media.SoundPlayer A = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/SelCorColocarA.wav");
                     A.Play();
                }
            }
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            if (_sensor != null)
            {
                _sensor.Stop();
            }
        }
        #endregion Carregar e fechar janelas 

        #region Eventos
         Classes.ContaPontos contaPontos = new Classes.ContaPontos();
        private void soltarCor_Click(object sender, RoutedEventArgs e)
        {
            if (_vermelhoClicado)
            {
                _objetoVermelhoClicado = true;
                Canvas.SetLeft(souLeVermelho, 629);
                Canvas.SetTop(souLeVermelho, 267);
                souLeVermelho.Visibility = System.Windows.Visibility.Hidden;
                LetraAVermelha.Visibility = System.Windows.Visibility.Visible;
                contaPontos.Acerto();
                _vermelhoClicado = false;
            }
            if (_azulClicado)
            {
                _objetoAzulClicado = true;
                Canvas.SetLeft(souLeAzul, 629);
                Canvas.SetTop(souLeAzul, 267);
                souLeAzul.Visibility = System.Windows.Visibility.Hidden;
                LetraAAzul.Visibility = System.Windows.Visibility.Visible;
               contaPontos.Erro(); ;
                _azulClicado = false;
            }
            if (_amareloClicado)
            {
                _objetoAmareloClicado = true;
                Canvas.SetLeft(souLeAmarela, 629);
                Canvas.SetTop(souLeAmarela, 267);
                souLeAmarela.Visibility = System.Windows.Visibility.Hidden;
                LetraAAmarela.Visibility = System.Windows.Visibility.Visible;
                contaPontos.Erro();
                _amareloClicado = false;
            }
            txtPonto.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Visibility = System.Windows.Visibility.Visible;
            txtPonto.Text = "Os pontos somados até agora são: ";
            txtPonto1.Text = Convert.ToString(contaPontos.SomaPontos);
           /* if (_objetoAzulClicado && _objetoAmareloClicado && _objetoVermelhoClicado)
            {
                DispatcherTimer dt = new DispatcherTimer();
                dt.Interval = TimeSpan.FromSeconds(1);
                dt.Tick += Dt_Tick;
                dt.Start();
           }*/
        }
        private void seleAzul_Click(object sender, RoutedEventArgs e)
        {
            HandCursor.Fill = new System.Windows.Media.ImageBrush(new System.Windows.Media.Imaging.BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Mao/Mao direita Azul.png", UriKind.Relative)));
            LetraAAzul.Visibility = System.Windows.Visibility.Hidden;
            LetraAAmarela.Visibility = System.Windows.Visibility.Hidden;
            LetraAVermelha.Visibility = System.Windows.Visibility.Hidden;
            if (_objetoAzulClicado == true)
            {
                seleAzul.IsEnabled = false;
            }
            else
            {
                seleAzul.Visibility = System.Windows.Visibility.Hidden;
                souLeAzul.Visibility = System.Windows.Visibility.Hidden;
                _azulClicado = true;
                if (_azulClicado == true)
                {
                    _amareloClicado = false;
                    _vermelhoClicado = false;
                    if (_objetoAmareloClicado == false)
                    {
                        seleAmarela.Visibility = System.Windows.Visibility.Visible;
                        souLeAmarela.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoAmareloClicado == true)
                    {
                        seleAmarela.IsEnabled = false;
                        seleAmarela.Visibility = System.Windows.Visibility.Hidden;
                    }
                    if (_objetoVermelhoClicado == false)
                    {
                        seleVermelho.Visibility = System.Windows.Visibility.Visible;
                        souLeVermelho.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoVermelhoClicado == true)
                    {
                        seleVermelho.IsEnabled = false;
                        seleVermelho.Visibility = System.Windows.Visibility.Hidden;
                    }
                }
                 System.Media.SoundPlayer selAzul = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/SeleCorAzul.wav");
                 selAzul.Play();
            }
        }
        private void seleAmarela_Click(object sender, RoutedEventArgs e)
        {
            HandCursor.Fill = new System.Windows.Media.ImageBrush(new System.Windows.Media.Imaging.BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Mao/Mao direita Amarela.png", UriKind.Relative)));

            LetraAAzul.Visibility = System.Windows.Visibility.Hidden;
            LetraAAmarela.Visibility = System.Windows.Visibility.Hidden;
            LetraAVermelha.Visibility = System.Windows.Visibility.Hidden;
            if (_objetoAmareloClicado == true)
            {
                seleAmarela.IsEnabled = false;
            }
            else
            {
                seleAmarela.Visibility = System.Windows.Visibility.Hidden;
                souLeAmarela.Visibility = System.Windows.Visibility.Hidden;
                _amareloClicado = true;
                if (_amareloClicado == true)
                {
                    _azulClicado = false;
                    _vermelhoClicado = false;
                    if (_objetoVermelhoClicado == false)
                    {
                        seleVermelho.Visibility = System.Windows.Visibility.Visible;
                        souLeVermelho.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoVermelhoClicado == true)
                    {
                        seleVermelho.IsEnabled = false;
                        seleVermelho.Visibility = System.Windows.Visibility.Hidden;
                   
                    }
                    if (_objetoAzulClicado == false)
                    {
                        seleAzul.Visibility = System.Windows.Visibility.Visible;
                        souLeAzul.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoAzulClicado == true)
                    {
                        seleAzul.IsEnabled = false;
                        seleAzul.Visibility = System.Windows.Visibility.Hidden;
                    }
                }
                 System.Media.SoundPlayer selAmarelo = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/SeleCorAmarela.wav");
                 selAmarelo.Play();
            }
        }
        private void seleVermelho_Click(object sender, RoutedEventArgs e)
        {
            HandCursor.Fill = new System.Windows.Media.ImageBrush(new System.Windows.Media.Imaging.BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Mao/Mao direita Vermelha.png", UriKind.Relative)));
            LetraAAzul.Visibility = System.Windows.Visibility.Hidden;
            LetraAAmarela.Visibility = System.Windows.Visibility.Hidden;
            LetraAVermelha.Visibility = System.Windows.Visibility.Hidden;
            if (_objetoVermelhoClicado == true)
            {
                seleVermelho.IsEnabled = false;
            }
            else
            {
                _vermelhoClicado = true;
                seleVermelho.Visibility = System.Windows.Visibility.Hidden;
                souLeVermelho.Visibility = System.Windows.Visibility.Hidden;
                if (_vermelhoClicado == true)
                {
                    _amareloClicado = false;
                    _azulClicado = false;
                    if (_objetoAmareloClicado == false)
                    {
                        seleAmarela.Visibility = System.Windows.Visibility.Visible;
                        souLeAmarela.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoAmareloClicado == true)
                    {
                        seleAmarela.IsEnabled = false;
                        seleAmarela.Visibility = System.Windows.Visibility.Hidden;
                    }
                    if (_objetoAzulClicado == false)
                    {
                        seleAzul.Visibility = System.Windows.Visibility.Visible;
                        souLeAzul.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoAzulClicado == true)
                    {
                        seleAzul.IsEnabled = false;
                        seleAzul.Visibility = System.Windows.Visibility.Hidden;
                    }
                }
                System.Media.SoundPlayer SelVermelha = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/SeleCorVermelha.wav");
                SelVermelha.Play();
            }
        }
        private void Letra_Click(object sender, RoutedEventArgs e)
        {
            if (_letraClicada == true)
            {
                System.Media.SoundPlayer letra = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/PronunciaA.wav");
                letra.Play();
                _letraClicada = false;
            }
        }
        #endregion Eventos
        Nivel_1_1 n11 = new Nivel_1_1();
          private void Anterior_Click(object sender, RoutedEventArgs e)
        {

            
        }
        private void MenuPrincipal_Click(object sender, RoutedEventArgs e)
        {
                   }

        private void Proximo_Click(object sender, RoutedEventArgs e)
        {
          
             if (_janelaPosterior == true)
              {
                n11.Show();
                _janelaPosterior = false;
             
            }
            _letraClicada = false;
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
           
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
        
        }
    }
}

