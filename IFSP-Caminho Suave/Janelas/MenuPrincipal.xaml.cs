﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
//adicionados as seguintes bibliotecas:
using Coding4Fun.Kinect.Wpf.Controls;
using System.Windows.Threading;
using Microsoft.Kinect;

namespace IFSP_Caminho_Suave.Janelas
{
    /// <summary>
    /// Interaction logic for MenuPrincipal.xaml
    /// </summary>
    public partial class MenuPrincipal : Window
    {
        #region Atributos
        private bool _AEIOU = true;
        private bool _completeRosto = true;
        private bool _completeCorpo = true;
        private static int larguraEcrã = 1500;
        private static int alturaTela = 1000;
        private KinectSensor _sensor;
        private Skeleton[] todosEsqueletos = new Skeleton[6];
        private List<HoverButton> botoesKinect;
        #endregion Atributos

        #region Janela principal
        public MenuPrincipal()
        {
            InitializeComponent();
            botoesKinect = new List<HoverButton>()
            {
                seleAEIOU,
                seleCorpo,
                seleRosto,
               
            };
         //   this.Closing += new System.ComponentModel.CancelEventHandler(Window_Closing);

        }
        #endregion Janela principal

        #region Captura
        void _sensor_AllFramesReady(object sender, AllFramesReadyEventArgs e)
        {
            Skeleton first = metdodos.GetFirstSkeleton(e);
            if (first == null)
            {
                return;
            }
            metdodos.ScalePosition(HandCursor, first.Joints[JointType.HandRight]);
            ProcessGesture(first.Joints[JointType.HandRight]);
            GetCameraPoint(first, e);
        }
        Classes.MetodosEPropriedades metdodos = new Classes.MetodosEPropriedades();
        void GetCameraPoint(Skeleton first, AllFramesReadyEventArgs e)
        {
            using (DepthImageFrame depth = e.OpenDepthImageFrame())
            {
                if (depth == null || _sensor == null)
                {
                    return;
                }
                DepthImagePoint rightDepthPoint =
                    depth.MapFromSkeletonPoint(first.Joints[JointType.HandRight].Position);
                ColorImagePoint rightColorPoint =
                    depth.MapToColorImagePoint(rightDepthPoint.X * 6, rightDepthPoint.Y * 3,
                        ColorImageFormat.RgbResolution640x480Fps30);

                metdodos.CameraPosition(HandCursor, rightColorPoint);
                foreach (HoverButton hb in botoesKinect)
                {
                    metdodos.CheckButton(hb, HandCursor);
                }
            }
        }
        #endregion Captura

        #region Processamentos
        private void ProcessGesture(Joint rightHand)
        {
            SkeletonPoint point = new SkeletonPoint();
            point.X = metdodos.ScaleVector(larguraEcrã, rightHand.Position.X);
            point.Y = metdodos.ScaleVector(alturaTela, -rightHand.Position.Y);
            point.Z = rightHand.Position.Z;
            rightHand.Position = point;
          /*  if (_primeiroClicado && !_objetoPrimeiroClicado)
            {
                Canvas.SetLeft(souLePrimeira, rightHand.Position.X / 1);
                Canvas.SetTop(souLePrimeira, rightHand.Position.Y / 1.9);
            }
            if (_segundoClicado && !_objetoSegundoClicado)
            {
                Canvas.SetLeft(souLeSegunda, rightHand.Position.X / 1);
                Canvas.SetTop(souLeSegunda, rightHand.Position.Y / 1.9);
            }
            if (_terceiroClicado && !_objetoTerceiroClicado)
            {
                Canvas.SetLeft(souLeTerceira, rightHand.Position.X / 1);
                Canvas.SetTop(souLeTerceira, rightHand.Position.Y / 1.9);
            }
         */
        }
        #endregion Processamentos

        #region Carregar e fechar janelas 
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // this.Close();
         
            this.Top = 0;
            this.Left = 0;
            if (KinectSensor.KinectSensors.Count > 0)
            {
              _sensor = KinectSensor.KinectSensors[0];
                if (_sensor.Status == KinectStatus.Connected)
                {
                    _sensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
                    _sensor.AllFramesReady += new EventHandler<AllFramesReadyEventArgs>(_sensor_AllFramesReady);
                    var parameters = new TransformSmoothParameters
                    {
                        Smoothing = 0.3f,
                        Correction = 0.0f,
                        Prediction = 0.0f,
                        JitterRadius = 1.0f,
                        MaxDeviationRadius = 0.5f
                    };
                    _sensor.SkeletonStream.Enable(parameters);
                    _sensor.Start();
                    System.Media.SoundPlayer seleNivel = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/SeleNivel.wav");
                    seleNivel.Play();
                }
            }
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            if (_sensor != null)
            {
              //  _sensor.Stop();
            }
        }
        #endregion Carregar e fechar janelas 
        Nivel_1_0 n10 = new Nivel_1_0();
        Nivel_2_0 n20 = new Nivel_2_0();
        Nivel_2_2_Corpo n22 = new Nivel_2_2_Corpo();
        private void seleAEIOU_Click(object sender, RoutedEventArgs e)
        {
            if (_AEIOU == true)
            {
                n10.Show();
                _AEIOU = false;
                _completeCorpo = false;
                _completeRosto = false;
            }
        }

        private void seleRosto_Click(object sender, RoutedEventArgs e)
        {
            if (_completeRosto == true)
            {
                  n20.Show();
                
                _completeRosto = false;
                _AEIOU = false;
                _completeCorpo = false;
                
            }


          


        }

        private void seleCorpo_Click(object sender, RoutedEventArgs e)
        {
            if (_completeCorpo == true)
            {
                n22.Show();
                
                _AEIOU = false;
                _completeRosto = false;
                _completeRosto = false;
            }
        }

       
    }
}
