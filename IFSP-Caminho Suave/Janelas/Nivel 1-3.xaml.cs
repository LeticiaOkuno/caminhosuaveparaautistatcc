﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
//adicionados as seguintes bibliotecas:
using Coding4Fun.Kinect.Wpf.Controls;
using System.Windows.Threading;
using Microsoft.Kinect;

namespace IFSP_Caminho_Suave.Janelas
{
    /// <summary>
    /// Interaction logic for Nivel_1_3.xaml
    /// </summary>
    public partial class Nivel_1_3 : Window
    {
        #region Atributos
        private bool _terceiroClicado = false;
        private bool _primeiroClicado = false;
        private bool _segundoClicado = false;
        private bool _letraClicada = true;
        private bool _janelaAnterior = true;
        private bool _janelaPosterior = true;
        private bool _menuPrincipal = true;
        private bool _objetoTerceiroClicado = false;
        private bool _objetoPrimeiroClicado = false;
        private bool _objetoSegundoClicado = false;
        private static int larguraEcrã = 1500;
        private static int alturaTela = 1000;
        private KinectSensor _sensor;
        private Skeleton[] todosEsqueletos = new Skeleton[6];
        private List<HoverButton> botoesKinect;
        #endregion Atributos

        #region Janela principal
        public Nivel_1_3()
        {
            InitializeComponent();
            botoesKinect = new List<HoverButton>()
            {
                soltarCor,
                selePrimeira,
                seleSegunda,
                seleTerceira,
                Letra,
                Proximo,
                Anterior,
                MenuPrincipal
            };
            this.Closing += new System.ComponentModel.CancelEventHandler(Window_Closing);
        }

        #endregion Janela principal

        #region Captura
        void _sensor_AllFramesReady(object sender, AllFramesReadyEventArgs e)
        {
            Skeleton first = metdodos.GetFirstSkeleton(e);
            if (first == null)
            {
                return;
            }
            metdodos.ScalePosition(HandCursor, first.Joints[JointType.HandRight]);
            ProcessGesture(first.Joints[JointType.HandRight]);
            GetCameraPoint(first, e);
        }
        Classes.MetodosEPropriedades metdodos = new Classes.MetodosEPropriedades();
        void GetCameraPoint(Skeleton first, AllFramesReadyEventArgs e)
        {
            using (DepthImageFrame depth = e.OpenDepthImageFrame())
            {
                if (depth == null || _sensor == null)
                {
                    return;
                }
                DepthImagePoint rightDepthPoint =
                    depth.MapFromSkeletonPoint(first.Joints[JointType.HandRight].Position);
                ColorImagePoint rightColorPoint =
                    depth.MapToColorImagePoint(rightDepthPoint.X * 6, rightDepthPoint.Y * 3,
                        ColorImageFormat.RgbResolution640x480Fps30);

                metdodos.CameraPosition(HandCursor, rightColorPoint);
                foreach (HoverButton hb in botoesKinect)
                {
                    metdodos.CheckButton(hb, HandCursor);
                }
            }
        }
        #endregion Captura
        /*
        #region Tempo
        private int incremento;
        private void Dt_Tick(object sender, EventArgs e)
        {
            incremento++;


            if (incremento == 3)
            {
               Nivel_1_4 n14 = new Nivel_1_4();
                n14.ShowDialog();
            }
        }
        #endregion Tempo
    */
        #region Processamentos
        private void ProcessGesture(Joint rightHand)
        {
            SkeletonPoint point = new SkeletonPoint();
            point.X = metdodos.ScaleVector(larguraEcrã, rightHand.Position.X);
            point.Y = metdodos.ScaleVector(alturaTela, -rightHand.Position.Y);
            point.Z = rightHand.Position.Z;
            rightHand.Position = point;
            if (_primeiroClicado && !_objetoPrimeiroClicado)
            {
                Canvas.SetLeft(souLePrimeira, rightHand.Position.X / 1);
                Canvas.SetTop(souLePrimeira, rightHand.Position.Y / 1.9);
            }
            if (_segundoClicado && !_objetoSegundoClicado)
            {
                Canvas.SetLeft(souLeSegunda, rightHand.Position.X / 1);
                Canvas.SetTop(souLeSegunda, rightHand.Position.Y / 1.9);
            }
            if (_terceiroClicado && !_objetoTerceiroClicado)
            {
                Canvas.SetLeft(souLeTerceira, rightHand.Position.X / 1);
                Canvas.SetTop(souLeTerceira, rightHand.Position.Y / 1.9);
            }
            //profundidade
            //caso eu queira deixar ate determinada distancia 
            if (rightHand.Position.Z > 0.70)
            {
                txtAviso.Visibility = Visibility.Visible;
                txtAviso.Text = "Venha mais para frente";
            }
        }
        #endregion Processamentos

        #region Carregar e fechar janelas 
         private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Top = 0;
            this.Left = 0;
            //  this.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            txtPonto.Visibility = System.Windows.Visibility.Hidden;
            txtPonto1.Visibility = System.Windows.Visibility.Hidden;
            souLeTerceira.Visibility = System.Windows.Visibility.Hidden;
            souLePrimeira.Visibility = System.Windows.Visibility.Hidden;
            souLeSegunda.Visibility = System.Windows.Visibility.Hidden;
            LetraPrimeira.Visibility = System.Windows.Visibility.Hidden;
            LetraSegunda.Visibility = System.Windows.Visibility.Hidden;
            LetraTerceira.Visibility = System.Windows.Visibility.Hidden;
            if (KinectSensor.KinectSensors.Count > 0)
            {
                txtAviso.Visibility = Visibility.Hidden;
                _sensor = KinectSensor.KinectSensors[0];
                if (_sensor.Status == KinectStatus.Connected)
                {
                    _sensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
                    _sensor.AllFramesReady += new EventHandler<AllFramesReadyEventArgs>(_sensor_AllFramesReady);
                    var parameters = new TransformSmoothParameters
                    {
                        Smoothing = 0.3f,
                        Correction = 0.0f,
                        Prediction = 0.0f,
                        JitterRadius = 1.0f,
                        MaxDeviationRadius = 0.5f
                    };
                    _sensor.SkeletonStream.Enable(parameters);
                    _sensor.Start();
                      System.Media.SoundPlayer O = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/SelCorColocarO.wav");
                     O.Play();
                }
            }
        }
        private void Window_Closed_1(object sender, EventArgs e)
        {
            if (_sensor != null)
            {
                _sensor.Stop();
            }
        }
        #endregion Carregar e fechar janelas 

        #region Eventos
        Classes.ContaPontos contaPontos = new Classes.ContaPontos();
        private void soltarCor_Click(object sender, RoutedEventArgs e)
        {
            if (_primeiroClicado)
            {
                _objetoPrimeiroClicado = true;
                Canvas.SetLeft(souLePrimeira, 629);
                Canvas.SetTop(souLePrimeira, 267);
                souLePrimeira.Visibility = System.Windows.Visibility.Hidden;
                LetraPrimeira.Visibility = System.Windows.Visibility.Visible;
                contaPontos.Acerto();
                _primeiroClicado = false;
            }
            if (_segundoClicado)
            {
                _objetoSegundoClicado = true;
                Canvas.SetLeft(souLeSegunda, 629);
                Canvas.SetTop(souLeSegunda, 267);
                souLeSegunda.Visibility = System.Windows.Visibility.Hidden;
                LetraSegunda.Visibility = System.Windows.Visibility.Visible;
                contaPontos.Erro(); ;
                _segundoClicado = false;
            }
            if (_terceiroClicado)
            {
                _objetoTerceiroClicado = true;
                Canvas.SetLeft(souLeTerceira, 629);
                Canvas.SetTop(souLeTerceira, 267);
                souLeTerceira.Visibility = System.Windows.Visibility.Hidden;
                LetraTerceira.Visibility = System.Windows.Visibility.Visible;
                contaPontos.Erro();
                _terceiroClicado = false;
            }
            txtPonto.Visibility = System.Windows.Visibility.Visible;
            txtPonto1.Visibility = System.Windows.Visibility.Visible;
            txtPonto.Text = "Os pontos somados até agora são: ";
            txtPonto1.Text = Convert.ToString(contaPontos.SomaPontos);
            /*if (_objetoPrimeiroClicado && _objetoSegundoClicado && _objetoTerceiroClicado)
            {
                DispatcherTimer dt = new DispatcherTimer();
                dt.Interval = TimeSpan.FromSeconds(1);
                dt.Tick += Dt_Tick;
                dt.Start();
            }*/
        }
        private void selePrimeira_Click(object sender, RoutedEventArgs e)
        {
            HandCursor.Fill = new System.Windows.Media.ImageBrush(new System.Windows.Media.Imaging.BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Mao/Mao direita.png", UriKind.Relative)));
            LetraPrimeira.Visibility = System.Windows.Visibility.Hidden;
            LetraSegunda.Visibility = System.Windows.Visibility.Hidden;
            LetraTerceira.Visibility = System.Windows.Visibility.Hidden;
            if (_objetoPrimeiroClicado == true)
            {
                selePrimeira.IsEnabled = false;
            }
            else
            {
                selePrimeira.Visibility = System.Windows.Visibility.Hidden;
                souLePrimeira.Visibility = System.Windows.Visibility.Hidden;
                _primeiroClicado = true;
                if (_primeiroClicado == true)
                {
                    _segundoClicado = false;
                    _terceiroClicado = false;
                    if (_objetoSegundoClicado == false)
                    {
                        seleSegunda.Visibility = System.Windows.Visibility.Visible;
                        souLeSegunda.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoSegundoClicado == true)
                    {
                        seleSegunda.IsEnabled = false;
                        seleSegunda.Visibility = System.Windows.Visibility.Hidden;
                    }
                    if (_objetoTerceiroClicado == false)
                    {
                        seleTerceira.Visibility = System.Windows.Visibility.Visible;
                        souLeTerceira.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoTerceiroClicado == true)
                    {
                        seleTerceira.IsEnabled = false;
                        seleTerceira.Visibility = System.Windows.Visibility.Hidden;
                    }
                }
                System.Media.SoundPlayer selPrimeira = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/SeleCorBranca.wav");
                selPrimeira.Play();
            }
        }
        private void seleSegunda_Click(object sender, RoutedEventArgs e)
        {
            HandCursor.Fill = new System.Windows.Media.ImageBrush(new System.Windows.Media.Imaging.BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Mao/Mao direita Roxa.png", UriKind.Relative)));
            LetraPrimeira.Visibility = System.Windows.Visibility.Hidden;
            LetraSegunda.Visibility = System.Windows.Visibility.Hidden;
            LetraTerceira.Visibility = System.Windows.Visibility.Hidden;
            if (_objetoSegundoClicado == true)
            {
                seleSegunda.IsEnabled = false;
            }
            else
            {
                seleSegunda.Visibility = System.Windows.Visibility.Hidden;
                souLeSegunda.Visibility = System.Windows.Visibility.Hidden;
                _segundoClicado = true;
                if (_segundoClicado == true)
                {
                    _primeiroClicado = false;
                    _terceiroClicado = false;
                    if (_objetoPrimeiroClicado == false)
                    {
                        selePrimeira.Visibility = System.Windows.Visibility.Visible;
                        souLePrimeira.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoPrimeiroClicado == true)
                    {
                        selePrimeira.IsEnabled = false;
                        selePrimeira.Visibility = System.Windows.Visibility.Hidden;
                    }
                    if (_objetoTerceiroClicado == false)
                    {
                        seleTerceira.Visibility = System.Windows.Visibility.Visible;
                        souLeTerceira.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoTerceiroClicado == true)
                    {
                        seleTerceira.IsEnabled = false;
                        seleTerceira.Visibility = System.Windows.Visibility.Hidden;
                    }

                }
                System.Media.SoundPlayer selSegunda = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/SeleCorRoxa.wav");
                selSegunda.Play();
            }
        }

        private void seleTerceira_Click(object sender, RoutedEventArgs e)
        {
            HandCursor.Fill = new System.Windows.Media.ImageBrush(new System.Windows.Media.Imaging.BitmapImage(new Uri(System.AppDomain.CurrentDomain.BaseDirectory + "Mao/Mao direita Cinza.png", UriKind.Relative)));
            LetraPrimeira.Visibility = System.Windows.Visibility.Hidden;
            LetraSegunda.Visibility = System.Windows.Visibility.Hidden;
            LetraTerceira.Visibility = System.Windows.Visibility.Hidden;
            if (_objetoTerceiroClicado == true)
            {
                seleTerceira.IsEnabled = false;
            }
            else
            {
                _terceiroClicado = true;
                seleTerceira.Visibility = System.Windows.Visibility.Hidden;
                souLeTerceira.Visibility = System.Windows.Visibility.Hidden;
                if (_terceiroClicado == true)
                {
                    _primeiroClicado = false;
                    _segundoClicado = false;
                    if (_objetoPrimeiroClicado == false)
                    {
                        selePrimeira.Visibility = System.Windows.Visibility.Visible;
                        souLePrimeira.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoPrimeiroClicado == true)
                    {
                        selePrimeira.IsEnabled = false;
                        selePrimeira.Visibility = System.Windows.Visibility.Hidden;
                    }
                    if (_objetoSegundoClicado == false)
                    {
                        seleSegunda.Visibility = System.Windows.Visibility.Visible;
                        souLeSegunda.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else if (_objetoSegundoClicado == true)
                    {
                        seleSegunda.IsEnabled = false;
                        seleSegunda.Visibility = System.Windows.Visibility.Hidden;
                    }
                }
                System.Media.SoundPlayer SelTerceira = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/SeleCorCinza.wav");
                SelTerceira.Play();
            }
        }
        private void Letra_Click(object sender, RoutedEventArgs e)
        {
            if (_letraClicada == true)
            {
                System.Media.SoundPlayer letra = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/PronunciaO.wav");
                letra.Play();
                _letraClicada = false;

            }
        }
        #endregion Eventos
        Nivel_1_4 n14 = new Nivel_1_4();
       
        private void Anterior_Click(object sender, RoutedEventArgs e)
        {
      //      n14.ShowDialog();
        }

        private void MenuPrincipal_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Proximo_Click(object sender, RoutedEventArgs e)
        {
            if (_janelaPosterior == true)
            {
               n14.Show();
                _janelaPosterior = false;
                _letraClicada = false;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
           // n14.ShowDialog();
        }
    }
}
