﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IFSP_Caminho_Suave.Classes
{
    class ContaPontos
    {
        //As variaveis estão estaticas para que quando mude de tela a pontuacao acompanhe o usuario
        #region Variaveis
        private static int pontosObtidos;
        private static int somaPontos;
        private static int totalPontos;
        #endregion Variaveis
        //Os metodos abaixo permitem a contagem dos pontos obtidos
        #region Metodos
        public int PontosObtidos
        {
            get
            {
                return pontosObtidos;
            }

            set
            {
                pontosObtidos = value;
            }
        }

        public int SomaPontos
        {
            get
            {
                return somaPontos;
            }

            set
            {
                somaPontos = value;
            }
        }

        public int TotalPontos
        {
            get
            {
                return totalPontos;
            }

            set
            {
                totalPontos = value;
            }
        }

        public void TudoClicado()
        {
            // int pontos;
            if (totalPontos == 0)
            {
                System.Media.SoundPlayer pal = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/Coloque.wav");

                pal.Play();
            }
            else if (totalPontos == 3)
            {
                System.Media.SoundPlayer pal = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/Coloque.wav");

                pal.Play();
            }
            else if (totalPontos == 9)
            {
                System.Media.SoundPlayer pal = new System.Media.SoundPlayer(System.AppDomain.CurrentDomain.BaseDirectory + "Midias/Coloque.wav");

                pal.Play();
            }


        }

        public void Acerto()
        {
            somaPontos = somaPontos + 3;
        }

        public void Erro()
        {
            somaPontos = somaPontos + 0;
        }

        public int Total()
        {
            totalPontos = somaPontos;

            return totalPontos;

        }

        #endregion Metodos

    }
}
