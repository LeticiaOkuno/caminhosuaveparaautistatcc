﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using Coding4Fun.Kinect.Wpf.Controls;
using Microsoft.Kinect;


namespace IFSP_Caminho_Suave.Classes
{
    class MetodosEPropriedades
    {
        #region Atibutos
        private static double _limiteSuperior;
        private static double _limiteInferior;
        private static double _limiteEsquerdo;
        private static double _limiteDireito;
        private static double _itemEsquerdo;
        private static double _itemSuperior;
        private static int larguraEcrã = 1500;
        private static int alturaTela = 1000;
        private Skeleton[] todosEsqueletos = new Skeleton[6];
        #endregion Atibutos

        #region Propriedades
        public static double LimiteSuperior
        {
            get
            {
                return _limiteSuperior;
            }

            set
            {
                _limiteSuperior = value;
            }
        }

        public static double LimiteInferior
        {
            get
            {
                return _limiteInferior;
            }

            set
            {
                _limiteInferior = value;
            }
        }

        public static double LimiteEsquerdo
        {
            get
            {
                return _limiteEsquerdo;
            }

            set
            {
                _limiteEsquerdo = value;
            }
        }

        public static double LimiteDireito
        {
            get
            {
                return _limiteDireito;
            }

            set
            {
                _limiteDireito = value;
            }
        }

        public static double ItemEsquerdo
        {
            get
            {
                return _itemEsquerdo;
            }

            set
            {
                _itemEsquerdo = value;
            }
        }

        public static double ItemSuperior
        {
            get
            {
                return _itemSuperior;
            }

            set
            {
                _itemSuperior = value;
            }
        }

        public static int LarguraEcrã
        {
            get
            {
                return larguraEcrã;
            }

            set
            {
                larguraEcrã = value;
            }
        }

        public static int AlturaTela
        {
            get
            {
                return alturaTela;
            }

            set
            {
                alturaTela = value;
            }
        }
        #endregion Propriedades

        #region Metodos dos botoes do kinect
        public void CheckButton(HoverButton botao, Ellipse thumbStick)
        {
            if (PontoMedio(botao, thumbStick))
            {
                botao.Hovering();
            }
            else
            {
                botao.Release();
            }
        }
        public bool PontoMedio(FrameworkElement container, FrameworkElement target)
        {
            FindValues(container, target);
            if (ItemSuperior < LimiteSuperior || LimiteInferior < ItemSuperior)
            {
                //O ponto médio do alvo está fora da parte superior ou inferior
                return false;
            }
            if (ItemEsquerdo < LimiteEsquerdo || LimiteDireito < ItemEsquerdo)
            {
                //O ponto médio do alvo está fora da esquerda ou da direita
                return false;
            }
            return true;
        }
        public void FindValues(FrameworkElement container, FrameworkElement target)
        {
            var containerTopLeft = container.PointToScreen(new Point());
            var itemTopLeft = target.PointToScreen(new Point());
            LimiteSuperior = containerTopLeft.Y;
            LimiteInferior = LimiteSuperior + container.ActualHeight;
            LimiteEsquerdo = containerTopLeft.X;
            LimiteDireito = LimiteEsquerdo + container.ActualWidth;
            //use midpoint of item (width or height divided by 2)
            ItemEsquerdo = itemTopLeft.X + (target.ActualWidth / 2);
            ItemSuperior = itemTopLeft.Y + (target.ActualHeight / 2);
        }
        #endregion Metodos dos botoes do kinect

        #region Metodos do esqueleto
        public Skeleton GetFirstSkeleton(AllFramesReadyEventArgs e)
        {
            using (SkeletonFrame skeletonFrameData = e.OpenSkeletonFrame())
            {
                if (skeletonFrameData == null)
                {
                    return null;
                }
                skeletonFrameData.CopySkeletonDataTo(todosEsqueletos);
                Skeleton first = (from s in todosEsqueletos
                                  where s.TrackingState == SkeletonTrackingState.Tracked
                                  select s).FirstOrDefault();
                return first;
            }
        }
        # endregion Metodos do esqueleto

        #region  Escalas e posiçoes
        //velocidade das letras
        public float ScaleVector(int length, float position)
        {
            float value = (((((float)length) / 1f) / 1f) * position) + (length / 1);
            if (value > length)
            {
                return (float)length;
            }
            if (value < 0f)
            {
                return 0f;
            }
            return value;
        }
        public void CameraPosition(FrameworkElement element, ColorImagePoint point)
        {
            Canvas.SetLeft(element, point.X - element.Width / 2);
            Canvas.SetTop(element, point.Y - element.Height / 2);
        }

        public void ScalePosition(FrameworkElement element, Joint joint)
        {   //convert the value to X/Y
            //Joint scaledJoint = joint.ScaleTo(1024, 768); 
            SkeletonPoint point = new SkeletonPoint();
            point.X = ScaleVector(LarguraEcrã, joint.Position.X);
            point.Y = ScaleVector(AlturaTela, -joint.Position.Y);
            point.Z = joint.Position.Z;
            Joint scaledJoint = joint;
            //Joint scaledJoint = joint.ScaleTo(1920, 1080);
            scaledJoint.TrackingState = JointTrackingState.Tracked;
            scaledJoint.Position = point;
            Canvas.SetLeft(element, scaledJoint.Position.X);
            Canvas.SetTop(element, scaledJoint.Position.Y);
        }
        #endregion Escalas e posiçoes
    }
}
